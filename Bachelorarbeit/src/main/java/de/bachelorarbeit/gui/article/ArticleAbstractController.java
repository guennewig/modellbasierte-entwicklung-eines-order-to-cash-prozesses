package de.bachelorarbeit.gui.article;

import de.bachelorarbeit.entity.ArticlePrice;
import de.bachelorarbeit.utilities.alert.CustomAlert;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

import java.io.File;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

/**
 * abstract article controller
 * extended by {@link ArticleController} and {@link EditArticleController}
 * summarize methods, both controller need
 */
public abstract class ArticleAbstractController {

    /**
     * selected picture for the article with the gui
     **/
    protected File selectedPicture;

    /**
     * method to open a file choose to select a picture and then set it to the gui
     *
     * @param pictureTextfield textfield where the result is shown
     */
    public void setPictureGUI(TextField pictureTextfield) {
        FileChooser pictureFileChooser = new FileChooser();
        //first call is empty, after that the directory is saved
        if (this.selectedPicture != null) {
            pictureFileChooser.setInitialDirectory(this.selectedPicture.getParentFile());
        }

        pictureFileChooser.setTitle("Bild für Artikel auswählen:");
        pictureFileChooser.getExtensionFilters().add(0, new FileChooser.ExtensionFilter("Bild (jpg, png)", "*.jpg", "*.png"));

        File file = pictureFileChooser.showOpenDialog(pictureTextfield.getScene().getWindow());
        if (file != null) {
            this.selectedPicture = file;
            pictureTextfield.setText(this.selectedPicture.getName());
        }
    }

    /**
     * method to add a price to the gui and model
     *
     * @param datePicker     date when price is valid
     * @param priceTextfield textfield where price is entered
     * @param priceTableView tableview where prices are shown
     * @param editMode       if its {@link ArticleController} or {@link EditArticleController}
     */
    protected void addPrice(DatePicker datePicker, TextField priceTextfield, TableView<ArticlePrice> priceTableView,
                            boolean editMode) {
        //when its edit mode, you cant add prices in the past or today, because it would manipulate the orders in the
        // past
        if (editMode && datePicker.getValue().isBefore(LocalDate.now().plusDays(1))) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler Preis hinzufügen!",
                    "Es darf kein Preis für die " + "Vergangenheit/Heute hinzugefügt" + " werden!",
                    "Fehler Preis " + "hinzufügen!");
            return;
        }

        //inputs must be present
        if (this.checkPriceInputsExists(datePicker, priceTextfield)) {
            //create new price
            ArticlePrice newPrice = new ArticlePrice(datePicker.getValue(),
                    Long.valueOf(priceTextfield.getText()));

            //check if theres already a price in the tableview for that date
            boolean existAlready = false;
            for (ArticlePrice price : priceTableView.getItems()) {
                if (newPrice.getValidityDate().equals(price.getValidityDate())) {
                    existAlready = true;
                    break;
                }
            }

            if (!existAlready) {
                //add price to gui and sort them after date
                priceTableView.getItems().add(newPrice);
                priceTableView.getItems().sort(Comparator.comparing(ArticlePrice::getValidityDate));
                //empty the input fields
                priceTextfield.setText("");
                datePicker.setValue(null);
            } else {
                CustomAlert.createAlert(Alert.AlertType.ERROR, "Datum bereits vorhanden!",
                        "Für dieses Datum wurde " + "bereits ein Preis eingetragen!", "Datum bereits vorhanden!");
            }

        }
    }

    /**
     * method to check if the input fields are filled
     *
     * @param datePicker     date
     * @param priceTextField price
     * @return true, when both are filled
     */
    protected boolean checkPriceInputsExists(DatePicker datePicker, TextField priceTextField) {
        if (datePicker.getValue() == null || priceTextField.getText().trim().isEmpty()) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehlerhafte Eingabe!", "Es wurde kein Preis/Datum " +
                    "eingegeben!", "Fehlerhafte Eingabe!");
            return false;
        }
        return true;
    }

    /**
     * checks if the prices are invalid -> there must be at least on price in the past or today
     *
     * @param prices
     * @return
     */
    protected boolean checkIfPricesInvalid(List<ArticlePrice> prices) {
        for (ArticlePrice price : prices) {
            if (price.getValidityDate().isBefore(LocalDate.now()) ||
                    price.getValidityDate().isEqual(LocalDate.now())) {
                return false;
            }
        }
        return true;
    }

    /**
     * checks if all inputs are filled to create an article
     *
     * @param articleName   textfield for article name
     * @param stockQuantity textfield for stock quantity
     * @param tableView     tableview prices table view
     * @return String error message, if everything is okay -> ""
     */
    protected String checkAllInputsPresent(String articleName, String stockQuantity,
                                           TableView<ArticlePrice> tableView) {
        if (articleName.isEmpty() || stockQuantity.isEmpty() || tableView.getItems().size() == 0) {
            return "Bitte füllen Sie alle Felder aus!";
        }
        //checken, ob mind. 1 Preis in der vergangenheit liegt
        else if (this.checkIfPricesInvalid(tableView.getItems())) {
            return "Es muss mindestens eine Preisangabe vorhanden sein bei der das Datum in der Vergangenheit liegt " + "oder gleich mit dem heutigen ist.";
        }
        return "";
    }

    /**
     * method to delete an article price
     * @param editMode if it is edit controller or not
     * @param price ArticlePrice
     * @param tableView TableView
     */
    protected void deleteArticlePrice(boolean editMode, ArticlePrice price, TableView<ArticlePrice> tableView){
        //if edit mode is active, it is not allowed to delete prices in the past
        if (editMode) {
            if (price.getValidityDate().isAfter(LocalDate.now())) {
                tableView.getItems().remove(price);
            } else {
                CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler Preis löschen!", "Es darf kein Preis " +
                        "gelöscht werden, der vor dem heutigen Datum liegt!", "Fehler Preis löschen!");
            }
        } else {
            tableView.getItems().remove(price);
        }
    }
}
