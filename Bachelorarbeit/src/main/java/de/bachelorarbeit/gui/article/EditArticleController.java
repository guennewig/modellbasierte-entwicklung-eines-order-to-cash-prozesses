package de.bachelorarbeit.gui.article;

import de.bachelorarbeit.database.DataBaseManager;
import de.bachelorarbeit.entity.Article;
import de.bachelorarbeit.entity.ArticlePrice;
import de.bachelorarbeit.gui.ActionButtonTableCell;
import de.bachelorarbeit.gui.PTableColumn;
import de.bachelorarbeit.utilities.alert.CustomAlert;
import de.bachelorarbeit.utilities.logging.LoggingManager;
import de.bachelorarbeit.utilities.prettyprint.StringPrettyPrinter;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * controller for edit-article.fxml
 */
public class EditArticleController extends ArticleAbstractController implements Initializable {

    /**
     * gui elements
     **/

    @FXML
    public TextField articleNameEditTextfield;

    @FXML
    public TextField stockQuantityEditTextfield;

    @FXML
    public TextField articlePictureEditTextfield;

    @FXML
    public DatePicker dateEditPicker;

    @FXML
    public TextField priceEditTextfield;

    @FXML
    public TableView<ArticlePrice> pricesEditTableView;

    @FXML
    public ImageView pictureEditImageview;

    private Article article;

    private ArticleController articleController;

    private boolean isPictureDeleted = false;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //set listener to textfields and build price table
        this.stockQuantityEditTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                this.stockQuantityEditTextfield.setText(oldValue);
            }
        });

        this.priceEditTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                this.priceEditTextfield.setText(oldValue);
            }
        });

        this.pricesEditTableView.setPlaceholder(new Label("Bitte mind. einen Preis für den Artikel eintragen!"));
        PTableColumn<ArticlePrice, String> dateColumn = new PTableColumn<>("Datum");
        dateColumn.setCellValueFactory(e -> new SimpleStringProperty(StringPrettyPrinter.prettyPrintDate(e.getValue().getValidityDate())));
        dateColumn.setPercentageWidth(0.4);

        PTableColumn<ArticlePrice, String> priceColumn = new PTableColumn<>("Preis (€)");
        priceColumn.setCellValueFactory(e -> new SimpleStringProperty(StringPrettyPrinter.prettyPrintLongToEuro(e.getValue().getPrice())));
        priceColumn.setPercentageWidth(0.4);

        PTableColumn<ArticlePrice, Button> deleteColumn = new PTableColumn<>("Löschen");
        deleteColumn.setCellFactory(ActionButtonTableCell.forTableColumn("de/bachelorarbeit/gui/images/trashcan" +
                ".png", (ArticlePrice price) -> {
            this.deleteArticlePrice(price);
        }));
        deleteColumn.setPercentageWidth(0.2);
        this.pricesEditTableView.getColumns().addAll(dateColumn, priceColumn, deleteColumn);
    }

    /**
     * method to delete an article price from gui
     *
     * @param price ArticlePrice
     */
    private void deleteArticlePrice(ArticlePrice price) {
        this.deleteArticlePrice(true, price, this.pricesEditTableView);
    }

    /**
     * method to set the article to the gui
     * called from {@link ArticleController}
     *
     * @param newArticle article to be set
     */
    public void setArticle(Article newArticle) {
        this.article = newArticle;

        this.articleNameEditTextfield.setText(newArticle.getArticleName());
        this.stockQuantityEditTextfield.setText(newArticle.getStockQuantity().toString());
        this.pricesEditTableView.getItems().addAll(newArticle.getPrices());
        this.article.setImageToView(this.pictureEditImageview);
    }

    /**
     * method to set the article controller, to call the search function
     *
     * @param newArticleController controller
     */
    public void setArticleController(ArticleController newArticleController) {
        this.articleController = newArticleController;
    }

    /**
     * method to select a picture
     */
    @FXML
    public void selectPicture() {
        this.setPictureGUI(this.articlePictureEditTextfield);
    }

    /**
     * method to add a price
     */
    @FXML
    public void addPrice() {
        this.addPrice(this.dateEditPicker, this.priceEditTextfield, this.pricesEditTableView, true);
    }

    /**
     * method to close the window
     */
    public void close() {
        ((Stage) this.articleNameEditTextfield.getScene().getWindow()).close();
    }

    /**
     * method to save the changes
     *
     * @throws IOException
     */
    @FXML
    public void accept() {
        //check if all inputs are present and valid
        String errorMessage = this.checkAllInputsPresent(this.articleNameEditTextfield.getText(),
                this.stockQuantityEditTextfield.getText(), this.pricesEditTableView);
        if (!errorMessage.equals("")) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Eingabefehler", errorMessage, "Fehlerhafte " + "Eingabe");
            return;
        }

        //if the article name is the same as the old one, no unique check must be done
        if ((this.article.getArticleName().equals(this.articleNameEditTextfield.getText().trim())) || (DataBaseManager.getInstance().isArticleNameUnique(this.articleNameEditTextfield.getText()))) {
            this.article.setArticleName(this.articleNameEditTextfield.getText().trim());
            this.article.setStockQuantity(Long.valueOf(this.stockQuantityEditTextfield.getText()));
            this.article.setDeleted(false);

            this.article.getPrices().clear();
            this.article.getPrices().addAll(this.pricesEditTableView.getItems());

            for (ArticlePrice price : this.article.getPrices()) {
                price.setArticle(this.article);
            }

            //set picture
            if (this.selectedPicture != null) {
                this.article.saveImage(this.selectedPicture);
            } else if (this.isPictureDeleted) {
                this.article.setArticlePicture(null);
            }

            //update the article
            DataBaseManager.getInstance().update(this.article);
            CustomAlert.createAlert(Alert.AlertType.INFORMATION, "Artikel bearbeiten",
                    "Artikel wurde erfolgreich " + "bearbeitet!", "Artikel bearbeitet!");
            LoggingManager.info("Artikel mit der ID: " + article.getArticleId() + " erfolgreich bearbeitet.");
            this.articleController.getAllArticlesFromSearch();
            this.close();
        } else {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Artikelbezeichnung", "Es ist bereits ein Artikel mit " +
                    "dieser Artikelbezeichnung " + "angelegt!", "Artikelbezeichnung vergeben!");
        }
    }

    /**
     * method to delete the selected picture
     */
    @FXML
    public void deletePictureSelection() {
        if (this.selectedPicture == null && this.articlePictureEditTextfield.getText().trim().equals("") && this.article.getArticlePicture() != null && this.article.getArticlePicture().length > 0 && !this.isPictureDeleted) {
            Optional<ButtonType> result = CustomAlert.createAlert(Alert.AlertType.CONFIRMATION, "Bild entfernen?",
                    "Es ist kein neues Bild angegeben, sodass das alte gelöscht werden würde. Soll dies geschehen?",
                    "Bild entfernen?");
            //if result is okay
            if (result.isPresent() && result.get().equals(ButtonType.OK)) {
                this.isPictureDeleted = true;
                this.pictureEditImageview.setImage(new Image(this.getClass().getClassLoader().getResourceAsStream("de" +
                        "/bachelorarbeit/gui" + "/images/no_photo.png")));
                CustomAlert.createAlert(Alert.AlertType.INFORMATION, "Artikelbild gelöscht!", "Das Artikelbild wurde " +
                        "gelöscht und muss noch bestätigt werden.", "Artikelbild gelöscht!");
            }
        } else {
            this.selectedPicture = null;
            this.articlePictureEditTextfield.setText("");
        }
    }
}
