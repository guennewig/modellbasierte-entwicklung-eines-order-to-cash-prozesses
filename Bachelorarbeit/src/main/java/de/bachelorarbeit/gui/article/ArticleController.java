package de.bachelorarbeit.gui.article;

import de.bachelorarbeit.database.DataBaseManager;
import de.bachelorarbeit.entity.Article;
import de.bachelorarbeit.entity.ArticlePrice;
import de.bachelorarbeit.gui.ActionButtonTableCell;
import de.bachelorarbeit.gui.PTableColumn;
import de.bachelorarbeit.utilities.alert.CustomAlert;
import de.bachelorarbeit.utilities.logging.LoggingManager;
import de.bachelorarbeit.utilities.prettyprint.StringPrettyPrinter;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;

/**
 * Controller for article-item-view.fxml
 */
public class ArticleController extends ArticleAbstractController implements Initializable {

    /**
     * gui elements
     **/

    @FXML
    public TextField articleNameTextfield;

    @FXML
    public TextField stockQuantityTextfield;

    @FXML
    public TextField articlePictureTextfield;

    @FXML
    public DatePicker datePicker;

    @FXML
    public TextField priceTextfield;

    @FXML
    public TableView<ArticlePrice> pricesTableView;

    @FXML
    public TableView<Article> articlesTableView;

    @FXML
    public TextField articleNameSearchTextfield;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        /** add all listener and build the table view **/
        this.stockQuantityTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                this.stockQuantityTextfield.setText(oldValue);
            }
        });

        this.priceTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                this.priceTextfield.setText(oldValue);
            }
        });

        this.pricesTableView.setPlaceholder(new Label("Bitte mind. einen Preis für den Artikel eintragen!"));
        PTableColumn<ArticlePrice, String> dateColumn = new PTableColumn<>("Datum");
        dateColumn.setCellValueFactory(e -> new SimpleStringProperty(StringPrettyPrinter.prettyPrintDate(e.getValue().getValidityDate())));
        dateColumn.setPercentageWidth(0.4);

        PTableColumn<ArticlePrice, String> priceColumn = new PTableColumn<>("Preis (€)");
        priceColumn.setCellValueFactory(e -> new SimpleStringProperty(StringPrettyPrinter.prettyPrintLongToEuro(e.getValue().getPrice())));
        priceColumn.setPercentageWidth(0.4);

        PTableColumn<ArticlePrice, Button> deleteColumn = new PTableColumn<>("Löschen");
        deleteColumn.setCellFactory(ActionButtonTableCell.forTableColumn("de/bachelorarbeit/gui/images/trashcan" +
                ".png", (ArticlePrice articlePrice) -> {
            this.deleteArticlePrice(articlePrice);
        }));
        deleteColumn.setPercentageWidth(0.2);
        this.pricesTableView.getColumns().addAll(dateColumn, priceColumn, deleteColumn);


        //build articles table view for search
        this.articlesTableView.setPlaceholder(new Label("Bitte Artikel über die Suche finden!"));
        PTableColumn<Article, String> idColumn = new PTableColumn<>("Artikel-ID");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("articleId"));
        idColumn.setPercentageWidth(0.1);

        PTableColumn<Article, String> articleNameColumn = new PTableColumn<>("Bezeichnung");
        articleNameColumn.setCellValueFactory(new PropertyValueFactory<>("articleName"));
        articleNameColumn.setPercentageWidth(0.295);

        PTableColumn<Article, String> stockQuantityColumn = new PTableColumn<>("Lagermenge");
        stockQuantityColumn.setCellValueFactory(new PropertyValueFactory<>("stockQuantity"));
        stockQuantityColumn.setPercentageWidth(0.295);

        PTableColumn<Article, String> priceSearchColumn = new PTableColumn<>("Preise");
        priceSearchColumn.setCellValueFactory(param -> {
            StringBuilder builder = new StringBuilder();
            for (ArticlePrice price : param.getValue().getPrices()) {
                builder.append(StringPrettyPrinter.prettyPrintDate(price.getValidityDate()));
                builder.append(" - ");
                builder.append(StringPrettyPrinter.prettyPrintLongToEuro(price.getPrice()));
                builder.append("\n");
            }
            return new SimpleStringProperty(builder.toString());
        });
        priceSearchColumn.setPercentageWidth(0.15);

        PTableColumn<Article, Button> changeColumn = new PTableColumn<>("Bearbeiten");
        changeColumn.setCellFactory(ActionButtonTableCell.<Article>forTableColumn("de/bachelorarbeit/gui/images/edit" + ".png", (Article article) -> {
            this.changeArticle(article);
        }));
        changeColumn.setPercentageWidth(0.08);

        PTableColumn<Article, Button> deleteArticleColumn = new PTableColumn<>("Löschen");
        deleteArticleColumn.setCellFactory(ActionButtonTableCell.<Article>forTableColumn("de/bachelorarbeit/gui" +
                "/images/trashcan.png", (Article article) -> {
            this.deleteArticleWithSearch(article);
        }));
        deleteArticleColumn.setPercentageWidth(0.08);

        this.articlesTableView.getColumns().addAll(idColumn, articleNameColumn, stockQuantityColumn,
                priceSearchColumn, changeColumn, deleteArticleColumn);
    }

    /**
     * method to delete an article price from gui
     *
     * @param price ArticlePrice
     */
    private void deleteArticlePrice(ArticlePrice price) {
        this.deleteArticlePrice(false, price, this.pricesTableView);
    }

    /**
     * method to select a picture
     */
    @FXML
    public void selectPicture() {
        this.setPictureGUI(this.articlePictureTextfield);
    }

    /**
     * method to add a price
     */
    @FXML
    public void addPrice() {
        this.addPrice(this.datePicker, this.priceTextfield, this.pricesTableView, false);
    }


    /**
     * method to create an article and save it into the database
     *
     * @throws IOException
     */
    @FXML
    public void createArticle() {
        //check all inputs
        String errorMessage = this.checkAllInputsPresent(this.articleNameTextfield.getText().trim(),
                this.stockQuantityTextfield.getText().trim(), this.pricesTableView);
        if (errorMessage.equals("")) {
            //check if article name is not taken
            if (DataBaseManager.getInstance().isArticleNameUnique(this.articleNameTextfield.getText())) {
                //deep copy of the list, otherwise it gets cleared when gui is cleared
                //add all prices to the article
                List<ArticlePrice> prices = new ArrayList<>();
                prices.addAll(this.pricesTableView.getItems());
                Article article = new Article(this.articleNameTextfield.getText(),
                        Long.valueOf(this.stockQuantityTextfield.getText()), prices);

                for (ArticlePrice price : prices) {
                    //when article price is created the article reference is null, it must be set for hibernate
                    price.setArticle(article);
                }

                //set picture
                if (this.selectedPicture != null) {
                    article.saveImage(this.selectedPicture);
                }

                //save the article
                DataBaseManager.getInstance().save(article);
                CustomAlert.createAlert(Alert.AlertType.INFORMATION, "Artikel anlegen",
                        "Artikel wurde erfolgreich " + "gespeichert!", "Artikel gespeichert!");
                LoggingManager.info("Artikel mit der ID: " + article.getArticleId() + " erfolgreich angelegt.");

                //clear the gui
                this.clearAllArticleInputs();
            } else {
                CustomAlert.createAlert(Alert.AlertType.ERROR, "Artikelbezeichnung", "Es ist bereits ein Artikel mit "
                        + "dieser Bezeichnung angelegt!", "Artikelbezeichnung vorhanden!");
            }
        } else {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Eingabefehler", errorMessage, "Fehlerhafte " + "Eingabe");
        }
    }

    /**
     * method to clear the gui
     */
    private void clearAllArticleInputs() {
        this.articleNameTextfield.setText("");
        this.stockQuantityTextfield.setText("");
        this.priceTextfield.setText("");
        this.articlePictureTextfield.setText("");
        this.priceTextfield.setText("");
        this.pricesTableView.getItems().clear();
        this.datePicker.setValue(null);
        this.selectedPicture = null;
    }

    /**
     * method to delete an article with search
     *
     * @param article Article to be deleted
     */
    private void deleteArticleWithSearch(Article article) {
        Optional<ButtonType> result = CustomAlert.createAlert(Alert.AlertType.CONFIRMATION, "Artikel löschen?", "Soll"
                + " " + "der Artikel mit den folgenden Informationen wirklich gelöscht werden?\n\n\t" + "ID: " + article.getArticleId() + "\n\tBezeichnung: " + article.getArticleName() + "\n\tLagermenge: " + article.getStockQuantity(), "Artikel löschen?");
        //if result is okay -> delete the article
        if (result.isPresent() && result.get().equals(ButtonType.OK)) {
            //delete it from the database
            DataBaseManager.getInstance().deleteByObject(article);
            //delete it from the view
            this.articlesTableView.getItems().remove(article);
            CustomAlert.createAlert(Alert.AlertType.INFORMATION, "Artikel gelöscht!",
                    "Der Artikel mit der ID: " + article.getArticleId() + " wurde erfolgreich gelöscht!",
                    "Artikel " + "gelöscht!");
            LoggingManager.info("Artikel mit der ID: " + article.getArticleId() + " erfolgreich gelöscht.");
        }
    }

    /**
     * method to get all articles from the search
     */
    @FXML
    public void getAllArticlesFromSearch() {
        //clear the gui table
        this.articlesTableView.getItems().clear();
        //get all articles from the database that matches the search
        List<Article> articles =
                DataBaseManager.getInstance().getAllArticlesFromSearch(this.articleNameSearchTextfield.getText().trim());
        if (articles.size() == 0) {
            CustomAlert.createAlert(Alert.AlertType.WARNING, "Artikelsuche", "Es konnten keine Artikel mit den " +
                    "angegebenen Kriterien gefunden werden!", "Keine Artikel gefunden!");
        } else {
            //sort founded articles and at it to the gui
            articles.sort(Comparator.comparing(Article::getArticleId));
            this.articlesTableView.getItems().addAll(articles);
        }
    }

    /**
     * method to open the edit view for an article
     *
     * @param article article to be changed
     */
    private void changeArticle(Article article) {
        FXMLLoader fxmlLoader = new FXMLLoader(ArticleController.class.getResource("edit-article-view.fxml"));
        try {
            Parent root = fxmlLoader.load();

            EditArticleController editArticleController = fxmlLoader.getController();
            editArticleController.setArticle(article);
            editArticleController.setArticleController(this);

            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);

            stage.setTitle("Artikel bearbeiten");

            stage.initOwner(this.articleNameTextfield.getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);

            stage.show();
        } catch (IOException e) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler beim Laden des Artikels", "Beim Laden des " +
                    "Artikels" + " " + "ist" + " ein Fehler aufgetreten!", "Fehler beim Laden des Artikels!");
            LoggingManager.log(Level.ALL, "Fehler beim Laden des Artikels mit der ID: " + article.getArticleId(),
                    e.getMessage());
        }
    }

    @FXML
    public void deletePictureSelection() {
        this.selectedPicture = null;
        this.articlePictureTextfield.setText("");
    }
}
