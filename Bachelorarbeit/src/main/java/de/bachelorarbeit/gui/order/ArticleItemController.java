package de.bachelorarbeit.gui.order;

import de.bachelorarbeit.entity.Article;
import de.bachelorarbeit.entity.ArticlePrice;
import de.bachelorarbeit.utilities.prettyprint.StringPrettyPrinter;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * controller for article-item-view.fxml
 * gui for one single article
 */
public class ArticleItemController {

    /**
     * gui elements
     **/

    @FXML
    private Label articleNameLabel;

    @FXML
    private ImageView articlePictureImageview;

    @FXML
    private Label stockQuantityLabel;

    @FXML
    private Label priceLabel;

    /**
     * the presendet article
     **/
    private Article article;

    /**
     * the order controller, used to set the selected article
     **/
    private OrderController orderController;

    /**
     * method to set the order controller
     *
     * @param newOrderController order controller
     */
    public void setOrderController(OrderController newOrderController) {
        this.orderController = newOrderController;
    }

    /**
     * method to set the data to the gui
     *
     * @param newArticle article to be set
     */
    public void setArticle(Article newArticle) {
        this.article = newArticle;
        this.articleNameLabel.setText(newArticle.getArticleName());
        this.articleNameLabel.setTooltip(new Tooltip(newArticle.getArticleName()));
        this.stockQuantityLabel.setText("Lagermenge: " + newArticle.getStockQuantity());
        ArticlePrice price = newArticle.getCurrentPrice();
        this.priceLabel.setText("Preis: " + StringPrettyPrinter.prettyPrintLongToEuro(price.getPrice()));
        this.article.setImageToView(this.articlePictureImageview);
    }

    /**
     * method the choose this article
     */
    @FXML
    public void chooseArticle() {
        this.orderController.setSelectedArticle(this.article);
        //when article is choosed close the window
        ((Stage) this.articleNameLabel.getScene().getWindow()).close();
    }

}
