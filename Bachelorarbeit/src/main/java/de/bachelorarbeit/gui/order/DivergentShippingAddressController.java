package de.bachelorarbeit.gui.order;

import de.bachelorarbeit.database.DataBaseManager;
import de.bachelorarbeit.entity.Address;
import de.bachelorarbeit.entity.PostalCode;
import de.bachelorarbeit.utilities.alert.CustomAlert;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * controller for divergent-shipping-address-view.fxml
 */
public class DivergentShippingAddressController implements Initializable {

    /** gui elements **/

    @FXML
    private TextField cityNameTextfield;

    @FXML
    private TextField postalCodeTextfield;

    @FXML
    private TextField streetTextfield;

    @FXML
    private TextField houseNumberTextfield;

    /**
     * order controller, used for setting the divergent shipping address
     */
    private OrderController orderController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //set the listener for the textfields
        this.postalCodeTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*") || newValue.length() > 5) {
                this.postalCodeTextfield.setText(oldValue);
            }
        });

        this.houseNumberTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[0-9]*[A-Z]?")) {
                houseNumberTextfield.setText(oldValue);
            }
        });
    }

    /**
     * set order controller
     * @param newOrderController order controller
     */
    public void setOrderController(OrderController newOrderController) {
        this.orderController = newOrderController;
    }

    /**
     * method to save the divergent shipping address
     */
    @FXML
    public void acceptAddress() {
        //check if all inputs are present
        String errorMessage = this.checkAllInputsPresent();
        if (!errorMessage.equals("")) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Eingabefehler", errorMessage, "Fehlerhafte " + "Eingabe");
            return;
        }
        //check if postalcode is present in database
        PostalCode postalCode = DataBaseManager.getInstance().getPostalCode(this.postalCodeTextfield.getText(),
                this.cityNameTextfield.getText());
        if (postalCode != null) {
            Address address = DataBaseManager.getInstance().checkAddress(new Address(postalCode,
                    this.streetTextfield.getText(), this.houseNumberTextfield.getText()));

            //set the divergent shipping address
            this.orderController.setDivergentShippingAddress(address);
            ((Stage) this.streetTextfield.getScene().getWindow()).close();
        }else{
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Stadt",
                    "Die Kombination aus Postleitzahl und Stadt ist " + "uns nicht bekannt!", "Stadt nicht gefunden!");
        }
    }

    /**
     * check if all inputs are present
     * @return true, if all inputs are present
     */
    private String checkAllInputsPresent() {
        if (this.cityNameTextfield.getText().trim().isEmpty() || this.postalCodeTextfield.getText().trim().isEmpty() || this.streetTextfield.getText().trim().isEmpty() || this.houseNumberTextfield.getText().trim().isEmpty()) {
            return "Bitte füllen Sie alle Felder aus!";
        } else if (!this.houseNumberTextfield.getText().matches("[0-9]+[A-Z]?")) {
            return "Die Hausnummer muss eine Zahl und kann direkt im Anschluss einen Buchstaben enthalten!";
        }
        return "";
    }
}
