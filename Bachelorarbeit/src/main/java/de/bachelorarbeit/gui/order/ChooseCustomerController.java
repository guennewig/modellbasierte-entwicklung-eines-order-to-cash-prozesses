package de.bachelorarbeit.gui.order;

import de.bachelorarbeit.utilities.alert.CustomAlert;
import de.bachelorarbeit.database.DataBaseManager;
import de.bachelorarbeit.entity.Customer;
import de.bachelorarbeit.gui.ActionButtonTableCell;
import de.bachelorarbeit.gui.PTableColumn;
import de.bachelorarbeit.utilities.prettyprint.StringPrettyPrinter;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

/**
 * controller for choosing a customer, choose-customer-view.fxml
 */
public class ChooseCustomerController implements Initializable {

    /** gui elements **/

    @FXML
    private TableView<Customer> customersTableView;

    @FXML
    private TextField firstNameSearchTextfield;

    @FXML
    private TextField lastNameSearchTextfield;

    @FXML
    private TextField emailSearchTextfield;

    /**
     * order controller, used to set the choosed customer
     */
    private OrderController orderController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //build the customer table view
        this.customersTableView.setPlaceholder(new Label("Bitte Kunden über die Suche finden!"));

        PTableColumn<Customer, String> idColumn = new PTableColumn<>("Kunden-ID");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("customerId"));
        idColumn.setPercentageWidth(0.08);

        PTableColumn<Customer, String> firstNameColumn = new PTableColumn<>("Vorname");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        firstNameColumn.setPercentageWidth(0.1);

        PTableColumn<Customer, String> lastNameColumn = new PTableColumn<>("Nachname");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        lastNameColumn.setPercentageWidth(0.1);

        PTableColumn<Customer, String> emailColumn = new PTableColumn<>("E-Mail");
        emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        emailColumn.setPercentageWidth(0.25);

        PTableColumn<Customer, String> dateOfBirthColumn = new PTableColumn<>("Geburtstag");
        dateOfBirthColumn.setCellValueFactory(e -> new SimpleStringProperty(StringPrettyPrinter.prettyPrintDate(e.getValue().getDateOfBirth())));
        dateOfBirthColumn.setPercentageWidth(0.1);

        PTableColumn<Customer, String> placeOfResidenceColumn = new PTableColumn<>("Wohnort");
        placeOfResidenceColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        placeOfResidenceColumn.setPercentageWidth(0.21);

        PTableColumn<Customer, Button> selectColumn = new PTableColumn<>("Auswählen");
        selectColumn.setCellFactory(ActionButtonTableCell.<Customer>forTableColumn("de/bachelorarbeit/gui/images/select.png", (Customer customer) -> {
            this.chooseCustomer(customer);
        }));
        selectColumn.setPercentageWidth(0.16);

        this.customersTableView.getColumns().addAll(idColumn, firstNameColumn, lastNameColumn, emailColumn, dateOfBirthColumn, placeOfResidenceColumn, selectColumn);
    }

    /**
     * method to set the order controller
     * @param newOrderController order controller
     */
    public void setOrderController(OrderController newOrderController) {
        this.orderController = newOrderController;
    }

    /**
     * method to get all customer from search
     */
    @FXML
    public void getAllCustomerFromSearch() {
        //clear customer table
        this.customersTableView.getItems().clear();
        //get all customer from database that match search
        List<Customer> customer =
                DataBaseManager.getInstance().getAllCustomersFromSearch(this.firstNameSearchTextfield.getText().trim(),
                        this.lastNameSearchTextfield.getText().trim(), this.emailSearchTextfield.getText().trim());
        if (customer.size() == 0) {
            CustomAlert.createAlert(Alert.AlertType.WARNING, "Kundensuche", "Es konnten keine Kunden mit den " +
                    "angegebenen Kriterien gefunden werden!", "Keine Kunden gefunden!");
        } else {
            //sort the customer by id and add to the gui
            customer.sort(Comparator.comparing(Customer::getCustomerId));
            this.customersTableView.getItems().addAll(customer);
        }
    }

    /**
     * method to choose the customer from the gui
     * @param customer customer to be choosen
     */
    public void chooseCustomer(Customer customer) {
        this.orderController.setSelectedCustomer(customer);
        ((Stage) this.firstNameSearchTextfield.getScene().getWindow()).close();
    }
}
