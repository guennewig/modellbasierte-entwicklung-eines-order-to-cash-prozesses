package de.bachelorarbeit.gui.order;

import de.bachelorarbeit.database.DataBaseManager;
import de.bachelorarbeit.entity.Customer;
import de.bachelorarbeit.entity.Order;
import de.bachelorarbeit.entity.OrderArticle;
import de.bachelorarbeit.gui.PTableColumn;
import de.bachelorarbeit.utilities.alert.CustomAlert;
import de.bachelorarbeit.utilities.logging.LoggingManager;
import de.bachelorarbeit.utilities.pdf.PDFOrderCreator;
import de.bachelorarbeit.utilities.prettyprint.StringPrettyPrinter;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * controller for editing an order, edit-order-view.fxml
 */
public class EditOrderController implements Initializable {

    /** gui elements **/

    @FXML
    private TextField customerEditTextfield;

    @FXML
    private TextField shippingAddressEditTextfield;

    @FXML
    private TextField orderDateEditTextfield;

    @FXML
    private TableView<OrderArticle> orderArticlesEditTableView;

    @FXML
    private CheckBox paidEditCheckbox;

    @FXML
    private TextField totalPriceEditTextfield;

    /** order to be edited **/
    private Order order;

    /** order controller, used for reloading the search **/
    private OrderController orderController;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //build article tableview
        this.orderArticlesEditTableView.setPlaceholder(new Label("Es müsste mind. ein Artikel angezeigt werden."));

        PTableColumn<OrderArticle, String> artikelIDColumn = new PTableColumn<>("Artikel-ID");
        artikelIDColumn.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getArticle().getArticleId().toString()));
        artikelIDColumn.setPercentageWidth(0.15);

        PTableColumn<OrderArticle, String> articleColumn = new PTableColumn<>("Artikel");
        articleColumn.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getArticle().getArticleName()));
        articleColumn.setPercentageWidth(0.45);

        PTableColumn<OrderArticle, String> priceColumn = new PTableColumn<>("Preis (€)");
        priceColumn.setCellValueFactory(e -> new SimpleStringProperty(StringPrettyPrinter.prettyPrintLongToEuro(e.getValue().getPeriodPrice())));
        priceColumn.setPercentageWidth(0.25);

        PTableColumn<OrderArticle, String> stockQuantityColumn = new PTableColumn<>("Menge");
        stockQuantityColumn.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getQuantity().toString()));
        stockQuantityColumn.setPercentageWidth(0.15);

        this.orderArticlesEditTableView.getColumns().addAll(artikelIDColumn, articleColumn, priceColumn, stockQuantityColumn);

        this.paidEditCheckbox.setDisable(true);
    }

    /**
     * method to set the order to the gui
     * @param newOrder order to be set
     */
    public void setOrder(Order newOrder) {
        this.order = newOrder;

        StringBuilder customerBuilder = new StringBuilder();
        Customer customer = newOrder.getCustomer();
        customerBuilder.append(customer.getCustomerId());
        customerBuilder.append(" - ");
        customerBuilder.append(customer.getLastName());
        customerBuilder.append(", ");
        customerBuilder.append(customer.getFirstName());
        customerBuilder.append(" - ");
        customerBuilder.append(StringPrettyPrinter.prettyPrintDate(customer.getDateOfBirth()));
        this.customerEditTextfield.setText(customerBuilder.toString());
        this.shippingAddressEditTextfield.setText(newOrder.getShippingAddress().toString());
        this.orderDateEditTextfield.setText(StringPrettyPrinter.prettyPrintDate(newOrder.getOrderDate()));
        this.paidEditCheckbox.setSelected(newOrder.isPaid());
        this.orderArticlesEditTableView.getItems().addAll(newOrder.getArticleList());
        this.totalPriceEditTextfield.setText(StringPrettyPrinter.prettyPrintLongToEuro(newOrder.getTotalPrice()));
    }

    /**
     * method to set the order controller
     * @param newOrderController order controller
     */
    public void setOrderController(OrderController newOrderController) {
        this.orderController = newOrderController;
    }

    /**
     * method to pay or unpay the order
     */
    public void setPaid() {
        //filler is for paid or not
        String filler = "";
        if (this.order.isPaid()) {
            //Zahlung wurde schon bezahlt
            filler = " nicht";
        }

        Optional<ButtonType> result = CustomAlert.createAlert(Alert.AlertType.CONFIRMATION, "Bestellung" + filler +
                " bezahlt?", "Soll diese Bestellung als" + filler + " bezahlt markiert werden?",
                "Bestellung" + filler + " bezahlt?");
        //if users presses okay
        if (result.isPresent() && result.get().equals(ButtonType.OK)) {
            //set the paid to the opposite
            this.order.setPaid(!this.order.isPaid());
            //update the database
            DataBaseManager.getInstance().update(this.order);
            CustomAlert.createAlert(Alert.AlertType.INFORMATION, "Bestellung" + filler + " bezahlt!", "Die Bestellung" +
                    " wurde als" + filler + " bezahlt markiert!", "Bestellung" + filler + " bezahlt!");
            LoggingManager.info("Bestellung mit der ID: " + order.getOrderId() + " wurde als" + filler + " bezahlt " +
                    "markiert.");
            this.paidEditCheckbox.setSelected(this.order.isPaid());
            this.orderController.getAllOrdersFromSearch();
        }
    }

    /**
     * method to generate the pdf bill
     */
    @FXML
    public void generateBill() {
        PDFOrderCreator.createOrderPDFMenu(this.order, (Stage) this.customerEditTextfield.getScene().getWindow());
    }
}
