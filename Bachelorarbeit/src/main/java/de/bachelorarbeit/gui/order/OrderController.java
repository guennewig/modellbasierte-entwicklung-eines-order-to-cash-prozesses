package de.bachelorarbeit.gui.order;

import de.bachelorarbeit.database.DataBaseManager;
import de.bachelorarbeit.entity.*;
import de.bachelorarbeit.gui.ActionButtonTableCell;
import de.bachelorarbeit.gui.PTableColumn;
import de.bachelorarbeit.utilities.alert.CustomAlert;
import de.bachelorarbeit.utilities.logging.LoggingManager;
import de.bachelorarbeit.utilities.mail.MailSender;
import de.bachelorarbeit.utilities.prettyprint.StringPrettyPrinter;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;

/**
 * controller for order-view.fxml
 */
public class OrderController implements Initializable {

    /**
     * gui elements
     **/

    @FXML
    public TextField customerTextfield;

    @FXML
    public TextField articleTextfield;

    @FXML
    public TextField quantityTextfield;

    @FXML
    public TableView<OrderArticle> orderArticlesTableView;

    @FXML
    public Label shippingAddressLabel;

    @FXML
    public CheckBox shippingAddressCheckbox;

    @FXML
    public TextField totalPriceTextfield;

    @FXML
    private TextField customerLastnameSearchTextfield;

    @FXML
    private DatePicker orderDateSearchPicker;

    @FXML
    private CheckBox paidSearchCheckbox;

    @FXML
    private TableView<Order> ordersTableView;

    /**
     * selected customer from choose customer controller
     **/
    private Customer selectedCustomer = null;

    /**
     * selected customer from choose article controller
     **/
    private Article selectedArticle = null;

    /**
     * selected customer from divergent shipping address controller
     **/
    private Address divergentShippingAddress = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //set listener and build article table
        this.quantityTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                quantityTextfield.setText(oldValue);
            }
        });

        this.orderArticlesTableView.setPlaceholder(new Label("Bitte mind. einen Artikel eintragen!"));

        PTableColumn<OrderArticle, String> articleColumn = new PTableColumn<>("Artikel");
        articleColumn.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getArticle().getArticleName()));
        articleColumn.setPercentageWidth(0.4);

        PTableColumn<OrderArticle, String> priceColumn = new PTableColumn<>("Preis (€)");
        priceColumn.setCellValueFactory(e -> new SimpleStringProperty(StringPrettyPrinter.prettyPrintLongToEuro(e.getValue().getPeriodPrice())));
        priceColumn.setPercentageWidth(0.2);

        PTableColumn<OrderArticle, String> quantityColumn = new PTableColumn<>("Anzahl");
        quantityColumn.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getQuantity().toString()));
        quantityColumn.setPercentageWidth(0.2);

        PTableColumn<OrderArticle, Button> deleteColumn = new PTableColumn<>("Löschen");
        deleteColumn.setCellFactory(ActionButtonTableCell.forTableColumn("de/bachelorarbeit/gui/images/trashcan" +
                ".png", (OrderArticle article) -> {
            this.deleteArticle(article);
        }));
        deleteColumn.setPercentageWidth(0.2);

        this.orderArticlesTableView.getColumns().addAll(articleColumn, priceColumn, quantityColumn, deleteColumn);

        //build order table for search
        this.ordersTableView.setPlaceholder(new Label("Bitte Bestellung über die Suche finden!"));

        PTableColumn<Order, String> orderIDColumn = new PTableColumn<>("Bestellung-ID");
        orderIDColumn.setCellValueFactory(new PropertyValueFactory<>("orderId"));
        orderIDColumn.setPercentageWidth(0.1);

        PTableColumn<Order, String> customerColumn = new PTableColumn<>("Kunde (ID)");
        customerColumn.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getCustomer().getFirstName() +
                " " + e.getValue().getCustomer().getLastName() + " (" + e.getValue().getCustomer().getCustomerId() +
                ")"));
        customerColumn.setPercentageWidth(0.25);

        PTableColumn<Order, String> shippingAddressColumn = new PTableColumn<>("Lieferadresse");
        shippingAddressColumn.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getShippingAddress().toString()));
        shippingAddressColumn.setPercentageWidth(0.3);

        PTableColumn<Order, String> orderDateColumn = new PTableColumn<>("Bestelldatum");
        orderDateColumn.setCellValueFactory(e -> new SimpleStringProperty(StringPrettyPrinter.prettyPrintDate(e.getValue().getOrderDate())));
        orderDateColumn.setPercentageWidth(0.15);

        PTableColumn<Order, Boolean> paidColumn = new PTableColumn<>("Bezahlt");
        paidColumn.setCellValueFactory(e -> new SimpleBooleanProperty(e.getValue().isPaid()));
        paidColumn.setCellFactory(e -> new CheckBoxTableCell<>());
        paidColumn.setPercentageWidth(0.1);

        PTableColumn<Order, Button> showColumn = new PTableColumn<>("Anzeigen");
        showColumn.setCellFactory(ActionButtonTableCell.<Order>forTableColumn("de/bachelorarbeit/gui/images" + "/show"
                + ".png", (Order order) -> {
            this.openOrder(order);
        }));
        showColumn.setPercentageWidth(0.1);

        this.ordersTableView.getColumns().addAll(orderIDColumn, customerColumn, shippingAddressColumn,
                orderDateColumn, paidColumn, showColumn);
    }

    /**
     * method to delte an article from view
     * @param article Article to be removed
     */
    private void deleteArticle(OrderArticle article){
        this.orderArticlesTableView.getItems().remove(article);
        this.totalPriceTextfield.setText(this.calculateSumToString(this.orderArticlesTableView.getItems()));
    }

    /**
     * method to open a window to choose the customer
     */
    @FXML
    public void chooseCustomer() {
        FXMLLoader fxmlLoader = new FXMLLoader(OrderController.class.getResource("choose-customer-view.fxml"));
        try {
            Parent root = fxmlLoader.load();

            ChooseCustomerController chooseCustomerController = fxmlLoader.getController();
            chooseCustomerController.setOrderController(this);

            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);

            stage.setTitle("Kunden auswählen");

            stage.initOwner(this.customerTextfield.getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);

            stage.show();
        } catch (IOException e) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler bei der Auswahl des Kunden", "Bei der Auswahl des "
                    + "Kunden ist ein Fehler aufgetreten!", "Fehler bei der Auswahl des Kunden");
            LoggingManager.log(Level.ALL, "Fehler bei der Auswahl des Kunden", e.getMessage());
        }
    }

    /**
     * method to set the selected customer
     *
     * @param newCustomer selected customer
     */
    public void setSelectedCustomer(Customer newCustomer) {
        this.selectedCustomer = newCustomer;
        StringBuilder builder = new StringBuilder();
        builder.append(newCustomer.getLastName());
        builder.append(", ");
        builder.append(newCustomer.getFirstName());
        builder.append(" - ");
        builder.append(StringPrettyPrinter.prettyPrintDate(newCustomer.getDateOfBirth()));
        this.customerTextfield.setText(builder.toString());
    }

    /**
     * method to open a window to choose an article
     */
    @FXML
    public void chooseArticle() {
        FXMLLoader fxmlLoader = new FXMLLoader(OrderController.class.getResource("choose-article-view.fxml"));
        try {
            Parent root = fxmlLoader.load();

            ChooseArticleController chooseArticleController = fxmlLoader.getController();
            chooseArticleController.setOrderController(this);

            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);

            stage.setTitle("Artikel auswählen");

            stage.initOwner(this.customerTextfield.getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);

            stage.show();
        } catch (IOException e) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler bei der Auswahl des Artikels",
                    "Bei der Auswahl " + "des " + "Artikels ist ein Fehler aufgetreten!",
                    "Fehler bei der Auswahl " + "des" + " Artikels");
            LoggingManager.log(Level.ALL, "Fehler bei der Auswahl des Artikels", e.getMessage());
        }
    }

    /**
     * method to set the selected article
     *
     * @param article
     */
    public void setSelectedArticle(Article article) {
        this.selectedArticle = article;
        this.articleTextfield.setText(article.getArticleName() + " - Lagermenge: " + article.getStockQuantity());
    }

    /**
     * method to add an article
     */
    @FXML
    public void addArticle() {
        //article must be selected and quantity > 0
        if (this.selectedArticle != null && this.quantityTextfield.getText().trim().length() > 0) {
            Long quantity = Long.valueOf(this.quantityTextfield.getText());
            if (quantity > 0) {
                //Prüfen, ob Artikel schon einmal hinzugefügt wurde
                boolean articleExists = false;
                //check if article exists for this order
                for (OrderArticle article : this.orderArticlesTableView.getItems()) {
                    if (this.selectedArticle.getArticleId().equals(article.getArticle().getArticleId())) {
                        articleExists = true;
                    }
                }

                if (!articleExists) {
                    //check, dass angeforderte Menge nicht größer ist als Lagermenge
                    if (quantity <= this.selectedArticle.getStockQuantity()) {
                        ArticlePrice currentPrice = this.selectedArticle.getCurrentPrice();
                        if (currentPrice != null) {
                            //create order article and add to view
                            OrderArticle orderArticle = new OrderArticle(this.selectedArticle, quantity,
                                    currentPrice.getPrice());
                            this.orderArticlesTableView.getItems().add(orderArticle);

                            //clear the inputs
                            this.articleTextfield.setText("");
                            this.quantityTextfield.setText("");
                            this.selectedArticle = null;

                            //update the price
                            this.totalPriceTextfield.setText(this.calculateSumToString(this.orderArticlesTableView.getItems()));
                        } else {
                            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler Artikel hinzufügen",
                                    "Es konnte " + "kein passender Preis gefunden werden!", "Fehler Artikel " +
                                            "hinzufügen");
                        }
                    } else {
                        CustomAlert.createAlert(Alert.AlertType.ERROR, "Artikel hinzufügen", "Die angeforderte Menge "
                                + "liegt über der Lagermenge!", "Fehler beim Hinzufügen eines " + "Artikels!");
                    }
                } else {
                    CustomAlert.createAlert(Alert.AlertType.ERROR, "Artikel hinzufügen", "Dieser Artikel wurde " +
                            "bereits " + "hinzugefügt!", "Fehler beim Hinzufügen eines " + "Artikels!");
                }
            } else {
                CustomAlert.createAlert(Alert.AlertType.ERROR, "Artikel hinzufügen",
                        "Die Menge muss größer als 0 " + "sein!", "Fehler beim Hinzufügen eines " + "Artikels!");
            }
        } else {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Artikel hinzufügen",
                    "Es wurde kein Artikel ausgewählt " + "oder es wurde eine falsche Eingabe für die Anzahl " + "get"
                            + "ätigt!", "Fehler beim Hinzufügen eines " + "Artikels!");
        }
    }

    /**
     * method to calculate the current sum
     *
     * @param articles list of articles in this order
     * @return String of sum in EURO
     */
    private Long calculateSum(List<OrderArticle> articles) {
        Long sum = 0L;
        for (OrderArticle orderArticle : articles) {
            sum = sum + (orderArticle.getQuantity() * orderArticle.getPeriodPrice());
        }
        return sum;
    }

    private String calculateSumToString(List<OrderArticle> articles) {
        return StringPrettyPrinter.prettyPrintLongToEuro(this.calculateSum(articles));
    }


    /**
     * method to save the order into database
     */
    @FXML
    public void createOrder() {
        //Kunde und Liste der Artikel > 0 prüfen
        if (this.selectedCustomer != null && this.orderArticlesTableView.getItems().size() > 0) {
            //create the order
            Order order = new Order(this.selectedCustomer, LocalDateTime.now());

            if (this.divergentShippingAddress != null) {
                order.setShippingAddress(this.divergentShippingAddress);
            } else {
                order.setShippingAddress(this.selectedCustomer.getAddress());
            }

            //add all articles from table
            List<OrderArticle> orderArticleList = new ArrayList<>();
            orderArticleList.addAll(this.orderArticlesTableView.getItems());
            order.setArticleList(orderArticleList);

            for (OrderArticle orderArticle : order.getArticleList()) {
                orderArticle.setOrder(order);
            }

            //calculate sum
            order.setTotalPrice(this.calculateSum(order.getArticleList()));

            //save the order to database
            DataBaseManager.getInstance().save(order);
            CustomAlert.createAlert(Alert.AlertType.INFORMATION, "Bestellung anlegen", "Bestellung wurde erfolgreich "
                    + "gespeichert!", "Bestellung gespeichert!");
            LoggingManager.info("Bestellung mit der ID: " + order.getOrderId() + " erfolgreich angelegt.");
            this.clearOrderInputs();
            //send confirmation mail
            this.sendOrderConfirmationEmail(order);
            //reduce stock quantity
            DataBaseManager.getInstance().reduceStock(order.getArticleList());
        } else {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Bestellung anlegen", "Es wurde kein Kunde ausgewählt " +
                    "oder" + " keine Artikel hinzugeügt!", "Fehler Anlegen einer Bestellung!");
        }
    }

    /**
     * method to send confirmation mail
     *
     * @param order order to confirm
     */
    private void sendOrderConfirmationEmail(Order order) {
        if (MailSender.sendOrderConfirmationEmail(order)) {
            CustomAlert.createAlert(Alert.AlertType.INFORMATION, "Bestätigungsemail versandt!", "Es wurde eine " +
                    "Bestätigungsemail an: " + order.getCustomer().getEmail() + " versandt!", "Bestätigungsemail " +
                    "versandt!");
            LoggingManager.info("Für die Bestellung mit der ID: " + order.getOrderId() + " wurde eine " + "Best" +
                    "ätigungsemail an: " + order.getCustomer().getEmail() + " versandt.");
        }
    }

    /**
     * method to clear all order inputs
     */
    private void clearOrderInputs() {
        this.customerTextfield.setText("");
        this.articleTextfield.setText("");
        this.quantityTextfield.setText("");
        this.totalPriceTextfield.setText("");
        this.orderArticlesTableView.getItems().clear();
        this.selectedArticle = null;
        this.selectedCustomer = null;
        this.setDivergentShippingAddress(null);


    }

    /**
     * method to open a window for divergent shipping address
     */
    @FXML
    public void checkBoxShippingAddressChanged() {
        if (this.shippingAddressCheckbox.isSelected()) {
            //Fenster öffnen für Adresseingabe
            FXMLLoader fxmlLoader = new FXMLLoader(OrderController.class.getResource("divergent-shipping-address-view"
                    + ".fxml"));
            try {
                Parent root = fxmlLoader.load();

                DivergentShippingAddressController divergentShippingAddressController = fxmlLoader.getController();
                divergentShippingAddressController.setOrderController(this);

                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setScene(scene);

                stage.setTitle("Abweichende Lieferadresse eingeben");

                stage.setOnCloseRequest(event -> {
                    this.setDivergentShippingAddress(null);
                });

                stage.initOwner(this.shippingAddressLabel.getScene().getWindow());
                stage.initModality(Modality.APPLICATION_MODAL);

                stage.show();
            } catch (IOException e) {
                CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler Adresseingabe", "Beim Laden der Adresseingabe "
                        + "ist ein Fehler aufgetreten!", "Fehler Adresseingabe");
                LoggingManager.log(Level.ALL, "Fehler bei der Adresseingabe", e.getMessage());
            }
        } else {
            this.divergentShippingAddress = null;
            this.shippingAddressLabel.setText("");
        }

    }

    /**
     * method to set the divergent shipping address
     *
     * @param shippingAddress
     */
    public void setDivergentShippingAddress(Address shippingAddress) {
        this.divergentShippingAddress = shippingAddress;
        if (shippingAddress != null) {
            this.shippingAddressLabel.setText(shippingAddress.toString());
        } else {
            this.shippingAddressLabel.setText("");
            this.shippingAddressCheckbox.setSelected(false);
        }
    }

    /**
     * method to get all orders from search
     */
    @FXML
    public void getAllOrdersFromSearch() {
        this.ordersTableView.getItems().clear();
        List<Order> orders =
                DataBaseManager.getInstance().getAllOrdersFromSearch(
                        this.customerLastnameSearchTextfield.getText().trim(), this.orderDateSearchPicker.getValue(),
                        this.paidSearchCheckbox.isSelected());
        if (orders.size() == 0) {
            CustomAlert.createAlert(Alert.AlertType.WARNING, "Artikelsuche", "Es konnten keine Artikel mit den " +
                    "angegebenen Kriterien gefunden werden!", "Keine Artikel gefunden!");
        } else {
            orders.sort(Comparator.comparing(Order::getOrderId));
            this.ordersTableView.getItems().addAll(orders);
        }
    }

    /**
     * method to open an order to editing mode
     *
     * @param order order to be edited
     */
    private void openOrder(Order order) {
        FXMLLoader fxmlLoader = new FXMLLoader(OrderController.class.getResource("edit-order-view" + ".fxml"));
        try {
            Parent root = fxmlLoader.load();

            EditOrderController editOrderController = fxmlLoader.getController();
            editOrderController.setOrder(order);
            editOrderController.setOrderController(this);

            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);

            stage.setTitle("Bestellung bearbeiten");

            stage.initOwner(this.customerTextfield.getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);

            stage.show();
        } catch (IOException e) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler beim Laden der Bestellung", "Beim Laden der " +
                    "Bestellung" + " " + "ist" + " ein Fehler aufgetreten!", "Fehler beim Laden der Bestellung!");
            LoggingManager.log(Level.ALL, "Fehler beim Laden der Bestellung mit der ID: " + order.getOrderId(),
                    e.getMessage());
        }
    }
}
