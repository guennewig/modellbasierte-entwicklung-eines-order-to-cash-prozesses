package de.bachelorarbeit.gui.order;

import de.bachelorarbeit.utilities.alert.CustomAlert;
import de.bachelorarbeit.database.DataBaseManager;
import de.bachelorarbeit.entity.Article;
import de.bachelorarbeit.utilities.logging.LoggingManager;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;

/**
 * controller for choose-article-view.fxml
 * presents all articles found from search
 */
public class ChooseArticleController {

    /** gui elements **/

    @FXML
    private TextField articleNameSearchTextfield;

    @FXML
    private GridPane itemGridpane;

    /**
     * order controller, used for setting the choosed article
     */
    private OrderController orderController;

    /**
     * method to set the order controller
     * @param newOrderController order controller
     */
    public void setOrderController(OrderController newOrderController){
        this.orderController = newOrderController;
    }

    /**
     * method to get article from search
     */
    @FXML
    public void getAllArticleFromSearch() {
        //clear the gridpane
        this.itemGridpane.getChildren().clear();
        //get all articles from database that match the search
        List<Article> articleListe =
                DataBaseManager.getInstance().getAllArticlesFromSearch(this.articleNameSearchTextfield.getText().trim());
        if (articleListe.size() == 0) {
            CustomAlert.createAlert(Alert.AlertType.WARNING, "Artikelsuche", "Es konnten keine Artikel mit den " +
                    "angegebenen Kriterien gefunden werden!", "Keine Artikel gefunden!");
        } else {
            //remove articles with 0 stock quantity
            articleListe.removeIf(article -> article.getStockQuantity()==0);

            //sort all articles
            articleListe.sort(Comparator.comparing(Article::getArticleId));
            int column = 0;
            int row = 1;

            //load for every article the fxml and add it into the gui
            try{
                for(Article article : articleListe){
                    FXMLLoader fxmlLoader = new FXMLLoader(ChooseArticleController.class.getResource("article-item-view.fxml"));
                    AnchorPane anchorPane = fxmlLoader.load();
                    ArticleItemController articleItemController = fxmlLoader.getController();
                    articleItemController.setArticle(article);
                    articleItemController.setOrderController(this.orderController);

                    if(column == 4){
                        column = 0;
                        row++;
                    }
                    this.itemGridpane.add(anchorPane, column++, row);
                    //set grid width
                    this.itemGridpane.setMinWidth(Region.USE_COMPUTED_SIZE);
                    this.itemGridpane.setPrefWidth(Region.USE_COMPUTED_SIZE);
                    this.itemGridpane.setMaxWidth(Region.USE_PREF_SIZE);
                    //set grid height
                    this.itemGridpane.setMinHeight(Region.USE_COMPUTED_SIZE);
                    this.itemGridpane.setPrefHeight(Region.USE_COMPUTED_SIZE);
                    this.itemGridpane.setMaxHeight(Region.USE_PREF_SIZE);


                    GridPane.setMargin(anchorPane, new Insets(15));
                }
            }catch(IOException exception){
                CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler bei der Auswahl des Artikels",
                        "Bei der Auswahl " + "des " + "Artikels ist ein Fehler aufgetreten!", "Fehler bei der Auswahl des" +
                                " Artikels");
                LoggingManager.log(Level.ALL, "Fehler bei der Auswahl des Artikels", exception.getMessage());
            }
        }
    }
}
