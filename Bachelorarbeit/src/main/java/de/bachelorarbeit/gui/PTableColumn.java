package de.bachelorarbeit.gui;


import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.control.TableColumn;

/**
 * class to create a custom table view column with percentage
 * @param <S> – The type of the TableView generic type (i.e. S == TableView<S>)
 * @param <T> – The type of the content in all cells in this TableColumn.
 *
 * From: https://thierrywasyl.wordpress.com/2012/12/03/tableview-columns-width-using-percentage-in-javafx/
 */
public class PTableColumn<S, T> extends TableColumn<S, T> {

    //percentage of column
    private final DoubleProperty percentageWidth = new SimpleDoubleProperty(1);

    public PTableColumn(String name){
        this.setText(name);
        tableViewProperty().addListener((ov, t, t1) -> {
            if(PTableColumn.this.prefWidthProperty().isBound()){
                PTableColumn.this.prefWidthProperty().unbind();
            }

            PTableColumn.this.prefWidthProperty().bind(t1.widthProperty().multiply(PTableColumn.this.percentageWidth));
        });
    }

    public final DoubleProperty percentageWidthProperty(){
        return this.percentageWidth;
    }

    public final void setPercentageWidth(double value){
        if(value >= 0 && value <= 1){
            this.percentageWidthProperty().set(value);
        }else{
            throw new IllegalArgumentException("Die Prozentzahl muss zwischen 0 und 1 sein.");
        }

    }

}
