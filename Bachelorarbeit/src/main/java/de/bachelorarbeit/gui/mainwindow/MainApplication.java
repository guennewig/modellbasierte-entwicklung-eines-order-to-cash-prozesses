package de.bachelorarbeit.gui.mainwindow;

import de.bachelorarbeit.database.DataBaseManager;
import de.bachelorarbeit.utilities.mail.MailSender;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * Class which handles the start of the application, called by
 */
public class MainApplication extends Application {

    //coordinates for dragging the application
    private double x, y;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
        //startup the database
        DataBaseManager.startDataBase();
        //startup the mail client
        MailSender.login();

        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("main-application-view.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);

        //for dragging the application
        root.setOnMousePressed(event -> {
            x = event.getSceneX();
            y = event.getSceneY();
        });

        root.setOnMouseDragged(event -> {
            stage.setX(event.getScreenX() - x);
            stage.setY(event.getScreenY() - y);
        });

        stage.setScene(scene);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.show();
    }
}