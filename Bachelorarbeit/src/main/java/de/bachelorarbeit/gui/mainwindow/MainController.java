package de.bachelorarbeit.gui.mainwindow;

import de.bachelorarbeit.database.DataBaseManager;
import de.bachelorarbeit.utilities.logging.LoggingManager;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;

/**
 * controller for main-application-view.fxml
 */
public class MainController implements Initializable {

    /**
     * gui element
     */
    @FXML
    private BorderPane borderPane;

    /** the instances are saved to save inputs when switching scenes **/

    /**
     * instanc of the home
     **/
    private Parent homeRoot;

    /**
     * instanc of the home
     **/
    private Parent customerRoot;

    /**
     * instanc of the article
     **/
    private Parent articleRoot;

    /**
     * instanc of the order
     **/
    private Parent orderRoot;

    private double x, y;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //log the application start
        LoggingManager.info("Applikation gestartet.");
        //set home in the center
        this.callHomeUI();
    }

    /**
     * method to load a ui component
     *
     * @param ui path of fxml to be loaded
     * @return instanc of Parent
     */
    private Parent loadUI(String ui) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(MainController.class.getResource(ui));
            Parent root = fxmlLoader.load();

            //for dragging application
            root.setOnMousePressed(event -> {
                x = event.getSceneX();
                y = event.getSceneY();
            });
            root.setOnMouseDragged(event -> {
                this.borderPane.getScene().getWindow().setX(event.getScreenX() - x);
                this.borderPane.getScene().getWindow().setY(event.getScreenY() - y);
            });

            return root;
        } catch (IOException e) {
            LoggingManager.log(Level.ALL, "Beim Laden der UI ist ein Fehler aufgetreten!", e.getMessage());
            return null;
        }
    }

    /**
     * method to set the home in the center
     */
    @FXML
    public void callHomeUI() {
        if (this.homeRoot == null) {
            this.homeRoot = this.loadUI("/de/bachelorarbeit/gui/home/home-view.fxml");
        }
        this.borderPane.setCenter(this.homeRoot);
    }

    /**
     * method to set the home in the customer
     */
    @FXML
    public void callCustomerUI() {
        if (this.customerRoot == null) {
            this.customerRoot = this.loadUI("/de/bachelorarbeit/gui/customer/customer-view.fxml");
        }
        this.borderPane.setCenter(this.customerRoot);
    }

    /**
     * method to set the home in the article
     */
    @FXML
    public void callArticleUI() {
        if (this.articleRoot == null) {
            this.articleRoot = this.loadUI("/de/bachelorarbeit/gui/article/article-view.fxml");
        }
        this.borderPane.setCenter(this.articleRoot);
    }

    /**
     * method to set the home in the order
     */
    @FXML
    public void callOrderUI() {
        if (this.orderRoot == null) {
            this.orderRoot = this.loadUI("/de/bachelorarbeit/gui/order/order-view.fxml");
        }
        this.borderPane.setCenter(this.orderRoot);
    }

    /**
     * method to close the window
     */
    @FXML
    public void close() {
        DataBaseManager.shutDown();
        Stage stage = (Stage) this.borderPane.getScene().getWindow();
        stage.close();
    }
}