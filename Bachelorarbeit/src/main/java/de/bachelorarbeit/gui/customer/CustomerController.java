package de.bachelorarbeit.gui.customer;

import de.bachelorarbeit.database.DataBaseManager;
import de.bachelorarbeit.entity.Address;
import de.bachelorarbeit.entity.Customer;
import de.bachelorarbeit.entity.PostalCode;
import de.bachelorarbeit.gui.ActionButtonTableCell;
import de.bachelorarbeit.gui.PTableColumn;
import de.bachelorarbeit.utilities.alert.CustomAlert;
import de.bachelorarbeit.utilities.logging.LoggingManager;
import de.bachelorarbeit.utilities.prettyprint.StringPrettyPrinter;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;

/**
 * controller for customer-view.fxml
 */
public class CustomerController extends CustomerAbstractController implements Initializable {

    /** gui elements **/

    @FXML
    private TextField firstNameTextfield;

    @FXML
    private TextField lastNameTextfield;

    @FXML
    private TextField emailTextfield;

    @FXML
    private DatePicker dateOfBirthPicker;

    @FXML
    private TextField cityNameTextfield;

    @FXML
    private TextField postalCodeTextfield;

    @FXML
    private TextField streetTextfield;

    @FXML
    private TextField houseNumberTextfield;

    @FXML
    private TableView<Customer> customersTableView;

    @FXML
    private TextField firstNameSearchTextfield;

    @FXML
    private TextField lastNameSearchTextfield;

    @FXML
    private TextField emailSearchTextfield;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //set listener and build customer table for search
        this.postalCodeTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!(newValue.matches("\\d*")) || !(newValue.length() <= 5)) {
                this.postalCodeTextfield.setText(oldValue);
            }
        });

        this.houseNumberTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[0-9]*[A-Z]?")) {
                this.houseNumberTextfield.setText(oldValue);
            }
        });

        this.customersTableView.setPlaceholder(new Label("Bitte Kunden über die Suche finden!"));

        PTableColumn<Customer, String> idColumn = new PTableColumn<>("Kunden-ID");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("customerId"));
        idColumn.setPercentageWidth(0.08);

        PTableColumn<Customer, String> firstNameColumn = new PTableColumn<>("Vorname");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        firstNameColumn.setPercentageWidth(0.1);

        PTableColumn<Customer, String> lastNameColumn = new PTableColumn<>("Nachname");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        lastNameColumn.setPercentageWidth(0.1);

        PTableColumn<Customer, String> emailColumn = new PTableColumn<>("E-Mail");
        emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        emailColumn.setPercentageWidth(0.25);

        PTableColumn<Customer, String> dateOfBirthColumn = new PTableColumn<>("Geburtstag");
        dateOfBirthColumn.setCellValueFactory(e -> new SimpleStringProperty(StringPrettyPrinter.prettyPrintDate(e.getValue().getDateOfBirth())));
        dateOfBirthColumn.setPercentageWidth(0.1);

        PTableColumn<Customer, String> placeOfResidenceColumn = new PTableColumn<>("Wohnort");
        placeOfResidenceColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        placeOfResidenceColumn.setPercentageWidth(0.21);

        PTableColumn<Customer, Button> changeColumn = new PTableColumn<>("Bearbeiten");
        changeColumn.setCellFactory(ActionButtonTableCell.<Customer>forTableColumn("de/bachelorarbeit/gui/images/edit"
                + ".png", (Customer customer) -> {
            this.changeCustomer(customer);
        }));
        changeColumn.setPercentageWidth(0.08);

        PTableColumn<Customer, Button> deleteColumn = new PTableColumn<>("Löschen");
        deleteColumn.setCellFactory(ActionButtonTableCell.<Customer>forTableColumn("de/bachelorarbeit/gui/images" +
                "/trashcan.png", (Customer customer) -> {
            this.deleteCustomerWithSearch(customer);
        }));
        deleteColumn.setPercentageWidth(0.08);

        this.customersTableView.getColumns().addAll(idColumn, firstNameColumn, lastNameColumn, emailColumn,
                dateOfBirthColumn, placeOfResidenceColumn, changeColumn, deleteColumn);
    }

    /**
     * method to create and save a customer
     */
    @FXML
    public void createCustomer() {
        //check if inputs are present
        String errorMessage = this.checkAllInputsCustomerPresent(this.firstNameTextfield,
                this.lastNameTextfield, this.emailTextfield, this.cityNameTextfield, this.streetTextfield,
                this.postalCodeTextfield, this.houseNumberTextfield, this.dateOfBirthPicker);
        if (!errorMessage.equals("")) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Eingabefehler", errorMessage, "Fehlerhafte " + "Eingabe");
            return;
        }
        //check if postalcode and city exists
        PostalCode postalCode = DataBaseManager.getInstance().getPostalCode(this.postalCodeTextfield.getText(),
                this.cityNameTextfield.getText());
        //PLZ muss vorhanden sein
        if (postalCode != null) {
            //check if entered email is not already taken
            if (DataBaseManager.getInstance().isEmailUnique(this.emailTextfield.getText().trim())) {
                Address address = DataBaseManager.getInstance().checkAddress(new Address(postalCode,
                        this.streetTextfield.getText().trim(), this.houseNumberTextfield.getText().trim()));
                Customer customer = new Customer(address, this.firstNameTextfield.getText().trim(),
                        this.lastNameTextfield.getText().trim(), this.dateOfBirthPicker.getValue(),
                        this.emailTextfield.getText().trim());
                //save the customer to database
                DataBaseManager.getInstance().save(customer);
                CustomAlert.createAlert(Alert.AlertType.INFORMATION, "Kunde anlegen", "Kunde wurde erfolgreich " +
                        "gespeichert!", "Kunde gespeichert!");
                LoggingManager.info("Kunde mit der ID: " + customer.getCustomerId() + " erfolgreich angelegt.");
                //clear gui
                this.clearAllInputsCreateCustomer();
            } else {
                CustomAlert.createAlert(Alert.AlertType.ERROR, "E-Mail", "Es ist bereits ein Konto mit dieser E-Mail "
                        + "angelegt!", "E-Mail vergeben!");
            }
        } else {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Stadt",
                    "Die Kombination aus Postleitzahl und Stadt ist " + "uns nicht bekannt!", "Stadt nicht gefunden!");
        }
    }

    /**
     * method to clear the gui
     */
    private void clearAllInputsCreateCustomer() {
        this.firstNameTextfield.setText("");
        this.lastNameTextfield.setText("");
        this.emailTextfield.setText("");
        this.cityNameTextfield.setText("");
        this.postalCodeTextfield.setText("");
        this.streetTextfield.setText("");
        this.houseNumberTextfield.setText("");
        this.dateOfBirthPicker.setValue(null);
    }

    /**
     * method to get all customers from search
     */
    @FXML
    public void getAllCustomersFromSearch() {
        //clear the customer table
        this.customersTableView.getItems().clear();
        //get alle customers from database that match search
        List<Customer> customers =
                DataBaseManager.getInstance().getAllCustomersFromSearch(this.firstNameSearchTextfield.getText().trim()
                        , this.lastNameSearchTextfield.getText().trim(), this.emailSearchTextfield.getText().trim());
        if (customers.size() == 0) {
            CustomAlert.createAlert(Alert.AlertType.WARNING, "Kundensuche", "Es konnten keine Kunden mit den " +
                    "angegebenen Kriterien gefunden werden!", "Keine Kunden gefunden!");
        } else {
            //sort founded customers and add to gui
            customers.sort(Comparator.comparing(Customer::getCustomerId));
            this.customersTableView.getItems().addAll(customers);
        }
    }

    /**
     * method to delete a customer found with search
     * @param customer
     */
    private void deleteCustomerWithSearch(Customer customer) {
        Optional<ButtonType> result = CustomAlert.createAlert(Alert.AlertType.CONFIRMATION, "Kunde löschen?",
                "Soll " + "der Kunde mit den folgenden Informationen wirklich gelöscht werden?\n\n\t" + "ID: " + customer.getCustomerId() + "\n\tNamen: " + customer.getFirstName() + " " + customer.getLastName() + "\n\tE-Mail: " + customer.getEmail() + "\n\tGeburtstag: " + StringPrettyPrinter.prettyPrintDate(customer.getDateOfBirth()), "Kunde löschen?");
        if (result.isPresent() && result.get().equals(ButtonType.OK)) {
            //delete customer from database
            DataBaseManager.getInstance().deleteByObject(customer);
            //delete customer from view
            this.customersTableView.getItems().remove(customer);
            CustomAlert.createAlert(Alert.AlertType.INFORMATION, "Kunde gelöscht!",
                    "Der Kunde mit der ID: " + customer.getCustomerId() + " wurde erfolgreich gelöscht!", "Kunde " +
                            "gel" + "öscht!");
            LoggingManager.info("Kunde mit der ID: " + customer.getCustomerId() + " erfolgreich gelöscht.");
        }
    }

    /**
     * method to open a window to edit a customer
     * @param customer customer to be edited
     */
    private void changeCustomer(Customer customer) {
        FXMLLoader fxmlLoader = new FXMLLoader(CustomerController.class.getResource("edit-customer-view.fxml"));
        try {
            Parent root = fxmlLoader.load();

            EditCustomerController editCustomerController = fxmlLoader.getController();
            editCustomerController.setCustomer(customer);
            editCustomerController.setCustomerController(this);

            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);

            stage.setTitle("Kunden bearbeiten");

            stage.initOwner(this.firstNameTextfield.getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);

            stage.show();
        } catch (IOException e) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler beim Laden des Kunden",
                    "Beim Laden des Kunden " + "ist" + " ein Fehler aufgetreten!", "Fehler beim Laden des Kunden!");
            LoggingManager.log(Level.ALL, "Fehler beim Laden des Kunden mit der ID: " + customer.getCustomerId(), e.getMessage());
        }
    }
}
