package de.bachelorarbeit.gui.customer;

import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import java.time.LocalDate;

/**
 * abstract customer controller
 * extended by {@link CustomerController} and {@link EditCustomerController}
 * summarize methods, both controller need
 */
public abstract class CustomerAbstractController {

    /**
     * method to check if all inputs are present
     *
     * @param firstNameTextfield   textfield for firstname
     * @param lastNameTextfield    textfield for lastname
     * @param emailTextfield       textfield for email
     * @param cityTextfield        textfield for city
     * @param streetTextfield      textfield for street
     * @param postalCodeTextfield  textfield for postal code
     * @param houseNumberTextfield textfield for house number
     * @param dateOfBirthPicker datepicker for birthday
     * @return
     */
    protected String checkAllInputsCustomerPresent(TextField firstNameTextfield, TextField lastNameTextfield, TextField emailTextfield,
                                                   TextField cityTextfield, TextField streetTextfield, TextField postalCodeTextfield,
                                                   TextField houseNumberTextfield, DatePicker dateOfBirthPicker) {
        if (firstNameTextfield.getText().trim().isEmpty() || lastNameTextfield.getText().trim().isEmpty() ||
                emailTextfield.getText().trim().isEmpty() || cityTextfield.getText().trim().isEmpty() ||
                streetTextfield.getText().trim().isEmpty() || postalCodeTextfield.getText().trim().isEmpty() ||
                houseNumberTextfield.getText().trim().isEmpty() || dateOfBirthPicker.getValue() == null) {
            return "Bitte füllen Sie alle Felder aus!";
        } else if (dateOfBirthPicker.getValue().isAfter(LocalDate.now())) {
            return "Das Geburtsdatum muss vor dem heutigen Tag liegen!";
        } else if (!houseNumberTextfield.getText().matches("[0-9]+[A-Z]?")) {
            return "Die Hausnummer muss eine Zahl und kann direkt im Anschluss einen Buchstaben enthalten!";
        } else if (!emailTextfield.getText().matches("^(.+)@(.+)$")) {
            return "Die E-Mail hat ein falsches Format. Beispiel: max.mustermann@test.de!";
        }
        return "";
    }
}
