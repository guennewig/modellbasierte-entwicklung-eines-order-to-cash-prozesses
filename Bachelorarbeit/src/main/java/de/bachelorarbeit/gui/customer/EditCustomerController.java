package de.bachelorarbeit.gui.customer;

import de.bachelorarbeit.database.DataBaseManager;
import de.bachelorarbeit.entity.Address;
import de.bachelorarbeit.entity.Customer;
import de.bachelorarbeit.entity.PostalCode;
import de.bachelorarbeit.utilities.alert.CustomAlert;
import de.bachelorarbeit.utilities.logging.LoggingManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * controller for edit-customer-view.fxml
 */
public class EditCustomerController extends CustomerAbstractController implements Initializable {

    /**
     * gui elements
     **/

    @FXML
    private TextField firstNameEditTextfield;

    @FXML
    private TextField lastNameEditTextfield;

    @FXML
    private TextField emailEditTextfield;

    @FXML
    private DatePicker dateOfBirthEditPicker;

    @FXML
    private TextField cityNameEditTextfield;

    @FXML
    private TextField postalCodeEditTextfield;

    @FXML
    private TextField streetEditTextfield;

    @FXML
    private TextField houseNumberEditTextfield;

    private Customer customer;

    private CustomerController customerController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //add listener to textfields
        this.postalCodeEditTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*") || !(newValue.length() <= 5)) {
                this.postalCodeEditTextfield.setText(oldValue);
            }
        });

        this.houseNumberEditTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[0-9]*[A-Z]?")) {
                this.houseNumberEditTextfield.setText(oldValue);
            }
        });
    }

    /**
     * method to set a customer to the gui, called by {@link CustomerController}
     *
     * @param newCustomer customer to be set
     */
    public void setCustomer(Customer newCustomer) {
        this.customer = newCustomer;

        this.firstNameEditTextfield.setText(newCustomer.getFirstName());
        this.lastNameEditTextfield.setText(newCustomer.getLastName());
        this.emailEditTextfield.setText(newCustomer.getEmail());
        this.cityNameEditTextfield.setText(newCustomer.getAddress().getPostalCode().getCityName());
        this.postalCodeEditTextfield.setText(newCustomer.getAddress().getPostalCode().getPostalCode());
        this.streetEditTextfield.setText(newCustomer.getAddress().getStreet());
        this.houseNumberEditTextfield.setText(newCustomer.getAddress().getHouseNumber());
        this.dateOfBirthEditPicker.setValue(newCustomer.getDateOfBirth());
    }

    /**
     * method to save the changed customer
     */
    @FXML
    public void accept() {
        //check if inputs are present
        String errorMessage = this.checkAllInputsCustomerPresent(this.firstNameEditTextfield,
                this.lastNameEditTextfield, this.emailEditTextfield, this.cityNameEditTextfield, this.streetEditTextfield,
                this.postalCodeEditTextfield, this.houseNumberEditTextfield, this.dateOfBirthEditPicker);
        if (!errorMessage.equals("")) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Eingabefehler", errorMessage, "Fehlerhafte " + "Eingabe");
            return;
        }
        //check the postalcode
        PostalCode postalCode = DataBaseManager.getInstance().getPostalCode(this.postalCodeEditTextfield.getText(),
                this.cityNameEditTextfield.getText());
        //postal code must be present in database
        if (postalCode != null) {
            //if the customer has the same email than the old one, no need to check for unique email
            if ((this.customer.getEmail().equals(this.emailEditTextfield.getText().trim()))
                    || (DataBaseManager.getInstance().isEmailUnique(this.emailEditTextfield.getText().trim()))) {
                this.customer.setFirstName(this.firstNameEditTextfield.getText().trim());
                this.customer.setLastName(this.lastNameEditTextfield.getText().trim());
                this.customer.setEmail(this.emailEditTextfield.getText().trim());
                this.customer.setDateOfBirth(this.dateOfBirthEditPicker.getValue());

                Address address = DataBaseManager.getInstance().checkAddress(new Address(postalCode,
                        this.streetEditTextfield.getText(), this.houseNumberEditTextfield.getText()));
                this.customer.setAddress(address);

                //update the customer into the database
                DataBaseManager.getInstance().update(customer);
                CustomAlert.createAlert(Alert.AlertType.INFORMATION, "Kunde bearbeiten", "Kunde wurde erfolgreich " +
                        "bearbeitet!", "Kunde bearbeitet!");
                LoggingManager.info("Kunde mit der ID: " + customer.getCustomerId() + " erfolgreich bearbeitet.");
                this.customerController.getAllCustomersFromSearch();
                this.close();
            } else {
                CustomAlert.createAlert(Alert.AlertType.ERROR, "E-Mail", "Es ist bereits ein Konto mit dieser E-Mail "
                        + "angelegt!", "E-Mail vergeben!");
            }
        } else {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Stadt",
                    "Die Kombination aus Postleitzahl und Stadt ist " + "uns nicht bekannt!", "Stadt nicht gefunden!");
        }
    }

    /**
     * method to close the window
     */
    public void close() {
        ((Stage)this.firstNameEditTextfield.getScene().getWindow()).close();
    }

    /**
     * method to set the customer controller, needs to be set for calling the search function
     *
     * @param newCustomerController customer controller
     */
    public void setCustomerController(CustomerController newCustomerController) {
        this.customerController = newCustomerController;
    }
}
