package de.bachelorarbeit.gui;

import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

import java.util.function.Consumer;

/**
 * Custom table cell to show a button
 * @param <S> what entity is presented
 * From: https://stackoverflow.com/questions/29489366/how-to-add-button-in-javafx-table-view
 */
public class ActionButtonTableCell<S> extends TableCell<S, Button> {

    /** button which is shown in gui **/
    private final Button actionButton;

    public ActionButtonTableCell(String file, Consumer<S> function) {
        //create the button
        this.actionButton = new Button();
        //when button is clicked the function is accepted and the current item from the table view is passed
        this.actionButton.setOnAction(e -> {
            function.accept(this.getCurrentItem());
        });
        //button is set as graphic
        this.actionButton.setGraphic(new ImageView(new Image(this.getClass().getClassLoader().getResourceAsStream(file), 25, 25, false, true)));
    }

    /**
     * method to create a table cell, get called from outside
     * @param picture icon for the button
     * @param function function
     * @return table cell
     * @param <S> what entity is presented
     */
    public static <S> Callback<TableColumn<S, Button>, TableCell<S, Button>> forTableColumn(String picture, Consumer<S> function) {
        return param -> new ActionButtonTableCell<>(picture, function);
    }

    /**
     * method to get the current item from the table view
     * @return the current item
     */
    public S getCurrentItem() {
        return (S) getTableView().getItems().get(this.getIndex());
    }

   @Override
    public void updateItem(Button item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            this.setGraphic(null);
        } else {
            this.setGraphic(this.actionButton);
        }
    }
}
