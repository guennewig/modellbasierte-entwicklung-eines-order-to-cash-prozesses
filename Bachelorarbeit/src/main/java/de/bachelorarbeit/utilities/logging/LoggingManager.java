package de.bachelorarbeit.utilities.logging;

import java.io.IOException;
import java.util.logging.*;

/**
 * class to manage the logging of the application
 */
public class LoggingManager {

    /** logging manager instance, singleton pattern **/
    private static LoggingManager INSTANCE;

    /** the logger **/
    private Logger logger;

    private LoggingManager() {
        //create new logger
        this.logger = Logger.getLogger(LoggingManager.class.getName());
        //set the level of the logger to ALL
        this.logger.setLevel(Level.ALL);

        //handler is for file writing
        Handler fileHandler;
        try {
            //save file in same folder as application
            fileHandler = new FileHandler("./bachelorarbeit.log", true);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //show logs in simple format
        fileHandler.setFormatter(new SimpleFormatter());
        //set level of file handler to all, to save all
        fileHandler.setLevel(Level.ALL);

        //set handler
        this.logger.addHandler(fileHandler);
        //set config message
        this.logger.config("Logger erfolgreich initialisiert.");
    }

    /**
     * get instance, singleton pattern
     * @return isnatnce of logger
     */
    private static Logger getLoggerInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LoggingManager();
        }
        return LoggingManager.INSTANCE.logger;
    }

    /**
     * method to log a info
     * @param info message
     */
    public static void info(String info) {
        LoggingManager.getLoggerInstance().info(info);
    }

    /**
     * method to log a warning
     * @param warning message
     */
    public static void warning(String warning) {
        LoggingManager.getLoggerInstance().warning(warning);
    }

    /**
     * message to log a severe
     * @param error message
     */
    public static void severe(String error) {
        LoggingManager.getLoggerInstance().severe(error);
    }

    /**
     * method to log a message to a level
     * @param level level
     * @param message message
     */
    public static void log(Level level, String message) {
        LoggingManager.getLoggerInstance().log(level, message);
    }

    /**
     * method to log a message to a level with object (e.g. exception)
     * @param level level
     * @param message message
     * @param o object to log
     */
    public static void log(Level level, String message, Object o) {
        LoggingManager.getLoggerInstance().log(level, message, o);
    }
}
