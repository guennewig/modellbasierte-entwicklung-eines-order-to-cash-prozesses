package de.bachelorarbeit.utilities.prettyprint;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * class helper to pretty print strings
 */
public abstract class StringPrettyPrinter {

    /**
     * method to pretty print a date in formate DD.MM.YYYY
     * @param localDate date
     * @return String pretty print of date
     */
    public static String prettyPrintDate(LocalDate localDate){
        StringBuilder builder = new StringBuilder();
        if(localDate.getDayOfMonth() < 10){
            builder.append("0");
        }
        builder.append(localDate.getDayOfMonth()).append(".");
        if(localDate.getMonthValue() < 10){
            builder.append("0");
        }
        builder.append(localDate.getMonthValue()).append(".");
        builder.append(localDate.getYear());

        return builder.toString();
    }

    /**
     * method to pretty print a date in formate DD.MM.YYYY
     * @param localDateTime date
     * @return String pretty print of date
     */
    public static String prettyPrintDate(LocalDateTime localDateTime){
        StringBuilder builder = new StringBuilder();
        builder.append(StringPrettyPrinter.prettyPrintDate(localDateTime.toLocalDate()));

        builder.append(" ");
        builder.append(localDateTime.getHour());
        builder.append(":");
        if(localDateTime.getMinute() < 10){
            builder.append("0");
        }
        builder.append(localDateTime.getMinute());

        return builder.toString();
    }

    /**
     * method to pretty print a long in EURO format
     * @param sum long
     * @return String pretty print sum
     */
    public static String prettyPrintLongToEuro(Long sum){
        StringBuilder builder = new StringBuilder();
        builder.append(sum / 100);
        builder.append(",");
        long mod = sum % 100;
        if (mod < 10) {
            builder.append("0");
        }
        builder.append(mod);
        builder.append("€");
        return builder.toString();
    }
}
