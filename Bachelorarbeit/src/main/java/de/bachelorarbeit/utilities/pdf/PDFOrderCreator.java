package de.bachelorarbeit.utilities.pdf;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import de.bachelorarbeit.entity.Customer;
import de.bachelorarbeit.entity.Order;
import de.bachelorarbeit.entity.OrderArticle;
import de.bachelorarbeit.utilities.alert.CustomAlert;
import de.bachelorarbeit.utilities.logging.LoggingManager;
import de.bachelorarbeit.utilities.prettyprint.StringPrettyPrinter;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.logging.Level;

/**
 * Copyright [2004] [The Apache Software Foundation]
 * SPDX-License-Identifier: Apache-2.0
 * PDFBOX
 * <p>
 * Class to create custom pdfs for orders
 */
public abstract class PDFOrderCreator {

    /**
     * int to save the distance to the above edge, needs to be saved extern because method gives
     * {@link PDPageContentStream} back
     **/
    private static int DISTANCE_ABOVE = 0;

    /**
     * Methode um aus einer Bestellung eine PDF zu erzeugen und ein Menü zum Speichern zu öffnen
     *
     * @param order {@link Order}
     * @param stage {@link Stage}
     */
    public static void createOrderPDFMenu(Order order, Stage stage) {
        try {
            //create pdf
            PDDocument pdfOrder = PDFOrderCreator.createOrderPDF(order);

            //get file by file chooser
            FileChooser fileChooser = new FileChooser();
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF (*.pdf)", "*.pdf");
            fileChooser.getExtensionFilters().add(extFilter);
            File file = fileChooser.showSaveDialog(stage);
            if (file != null) {
                if (pdfOrder != null) {
                    //save the pdf
                    pdfOrder.save(file.getAbsolutePath());
                    CustomAlert.createAlert(Alert.AlertType.INFORMATION, "Speichern erfolgreich!", "Die Rechnung " +
                            "wurde erfolgreich gespeichert!", "Speichern erfolgreich!");
                    LoggingManager.info("Es wurde die PDF für die Bestellung: " + order.getOrderId() + " unter: " + file.getAbsolutePath() + " erfolgreich gespeichert!");
                } else {
                    CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler Rechnungserstellung!", "Bei der " +
                            "Generierung der Rechnung ist ein Fehler aufgetreten!", "Fehler Rechnungserstellung!");
                    LoggingManager.log(Level.ALL,
                            "Bei der Erstellung der PDF für die Bestellung: " + order.getOrderId() + " ist ein " +
                                    "Fehler" + " aufgetreten!");
                }
            }

            pdfOrder.close();
        } catch (IOException e) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler Rechnungserstellung!",
                    "Bei der Generierung der " + "Rechnung ist ein Fehler aufgetreten!", "Fehler Rechnungserstellung!");
            LoggingManager.log(Level.ALL, "Fehler bei der Rechnungserstellung!", e.getMessage());
        }
    }

    /**
     * method to create a pdf for email (temp path)
     *
     * @param order order to pdf
     * @return path of temp file
     */
    public static Path createOrderPDFForEmail(Order order) {
        try {
            //create PDF
            PDDocument pdfOrder = PDFOrderCreator.createOrderPDF(order);
            if (pdfOrder != null) {
                //create a temp path
                Path tempPath = Files.createTempFile("tempPDF", ".pdf");
                //save pdf in temp path
                pdfOrder.save(tempPath.toFile());
                pdfOrder.close();
                return tempPath;
            }
        } catch (IOException e) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler temporäre Dateierstellung!",
                    "Bei der Erstellung " + "einer temporären Datei ist ein Fehler aufgetreten!", "Fehler temporärer "
                            + "Dateierstellung!");
            LoggingManager.log(Level.ALL, "Fehler temporäre Dateierstellung!", e.getMessage());
        }
        return null;
    }

    /**
     * method to create a pdf from order
     *
     * @param order {@link Order}
     * @return PDF Document
     */
    private static PDDocument createOrderPDF(Order order) {
        //create new pdf
        PDDocument bill = new PDDocument();
        try {
            //add a page
            PDPage myPage = new PDPage(PDRectangle.A4);
            bill.addPage(myPage);

            //Contentstream open
            PDPageContentStream cont = new PDPageContentStream(bill, myPage);
            float height = myPage.getMediaBox().getHeight();
            float width = myPage.getMediaBox().getWidth();

            addText(cont, "BESTELLBESTÄTIGUNG BACHELORARBEIT", 20, height - 50, true, 12);

            //set distances
            int distanceAbove = 100;
            int distanceCustomerAbove = distanceAbove;
            int distanceCustomerLeft = 30;
            int distanceOrderAbove = distanceAbove;
            int distanceOrderLeft = 310;
            int distanceArticleListAbove = 250;
            int distanceArticleListLeft = 30;

            /**------------------------------ add picture ---------------------------------------------------*/
            InputStream inputStream =
                    PDFOrderCreator.class.getClassLoader().getResourceAsStream("de" + "/bachelorarbeit" + "/gui/images/shopping_cart.png");
            byte[] byteArray = IOUtils.toByteArray(inputStream);
            PDImageXObject image = PDImageXObject.createFromByteArray(bill, byteArray, "shopping_cart.jpg");
            image.setHeight(70);
            image.setWidth(70);
            cont.drawImage(image, width - 120, height - 80);

            /**------------------------------ add customer data -------------------------------------------*/
            addCustomerText(order.getCustomer(), cont, height, distanceCustomerAbove, distanceCustomerLeft);

            /**------------------------------ add order data -------------------------------------------*/
            addOrderText(order, cont, height, distanceOrderAbove, distanceOrderLeft);

            /**------------------------------ add article list -------------------------------------------*/
            //add rectangle
            cont.setNonStrokingColor(Color.DARK_GRAY);
            cont.fillRect(distanceArticleListLeft - 15, height - distanceArticleListAbove + 10, 530, 0.5F);
            cont.fillRect(distanceArticleListLeft - 15, height - distanceArticleListAbove - 5, 530, 0.5F);

            cont.setNonStrokingColor(Color.LIGHT_GRAY);
            cont.fillRect(distanceArticleListLeft - 15, height - distanceArticleListAbove - 4.5f, 530, 14.5f);

            cont.setNonStrokingColor(Color.BLACK);
            addText(cont, "BESTELLÜBERSICHT", distanceArticleListLeft, height - distanceArticleListAbove, true, 10);

            distanceArticleListAbove += 20;
            PDPageContentStream newCont = addArticleListText(order, bill, cont, height, distanceArticleListAbove,
                    distanceArticleListLeft);
            /**------------------------------ add total sum -------------------------------------------*/
            addTotalSum(bill, newCont, height, StringPrettyPrinter.prettyPrintLongToEuro(order.getTotalPrice()));

            cont.close();
            return bill;
        } catch (IOException e) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler Rechnungserstellung!",
                    "Bei der Generierung der " + "Rechnung ist ein Fehler aufgetreten!", "Fehler Rechnungserstellung!");
            LoggingManager.log(Level.ALL, "Fehler Rechnungserstellung!", e.getMessage());
        }
        return null;
    }

    /**
     * method to add a text
     *
     * @param cont     {@link PDPageContentStream}
     * @param text     String
     * @param x        x-Value
     * @param y        y-Value
     * @param bold     bold text?
     * @param fontsize font size
     * @throws IOException
     */
    private static void addText(PDPageContentStream cont, String text, float x, float y, boolean bold, int fontsize) throws IOException {
        //start of text
        cont.beginText();
        //if text should be bold
        if (bold) {
            cont.setFont(PDType1Font.TIMES_BOLD, fontsize);
        } else {
            cont.setFont(PDType1Font.TIMES_ROMAN, fontsize);
        }
        //add text
        cont.newLineAtOffset(x, y);
        cont.showText(text);
        cont.endText();
    }

    /**
     * method to add all customer data
     *
     * @param customer              {@link Customer}
     * @param cont                  {@link PDPageContentStream}
     * @param height                height of the page
     * @param distanceCustomerAbove distance to the top
     * @param distanceCustomerLeft  distance to the left
     * @throws IOException
     */
    private static void addCustomerText(Customer customer, PDPageContentStream cont, float height,
                                        int distanceCustomerAbove, int distanceCustomerLeft) throws IOException {
        //add rectangle
        cont.setNonStrokingColor(Color.DARK_GRAY);
        cont.fillRect(distanceCustomerLeft - 15, height - distanceCustomerAbove + 10, 250, 0.5F);
        cont.fillRect(distanceCustomerLeft - 15, height - distanceCustomerAbove - 5, 250, 0.5F);

        cont.setNonStrokingColor(Color.LIGHT_GRAY);
        cont.fillRect(distanceCustomerLeft - 15, height - distanceCustomerAbove - 4.5f, 250, 14.5f);

        cont.setNonStrokingColor(Color.BLACK);
        addText(cont, "DEINE DATEN", distanceCustomerLeft, height - distanceCustomerAbove, true, 10);

        //------------------------------------------------------------
        distanceCustomerAbove += 20;
        addText(cont, "NAME:", distanceCustomerLeft, height - distanceCustomerAbove, true, 8);
        addText(cont, customer.getFirstName() + " " + customer.getLastName(), distanceCustomerLeft + 80,
                height - distanceCustomerAbove, false, 8);

        //------------------------------------------------------------
        distanceCustomerAbove += 20;
        addText(cont, "E-MAIL:", distanceCustomerLeft, height - distanceCustomerAbove, true, 8);
        addText(cont, customer.getEmail(), distanceCustomerLeft + 80, height - distanceCustomerAbove, false, 8);

        //------------------------------------------------------------
        distanceCustomerAbove += 20;
        addText(cont, "WOHNADRESSE:", distanceCustomerLeft, height - distanceCustomerAbove, true, 8);
        addText(cont, customer.getAddress().getStreet() + " " + customer.getAddress().getHouseNumber(),
                distanceCustomerLeft + 80, height - distanceCustomerAbove, false, 8);
        addText(cont, customer.getAddress().getPostalCode().toString(), distanceCustomerLeft + 80,
                height - distanceCustomerAbove - 20, false, 8);
    }

    /**
     * method to add all order data
     *
     * @param order              {@link Order}
     * @param cont               {@link PDPageContentStream}
     * @param height             height of page
     * @param distanceOrderAbove distance to the top
     * @param distanceOrderLeft  distance to the left
     * @throws IOException
     */
    private static void addOrderText(Order order, PDPageContentStream cont, float height, int distanceOrderAbove,
                                     int distanceOrderLeft) throws IOException {
        //add rectangle
        cont.setNonStrokingColor(Color.DARK_GRAY);
        cont.fillRect(distanceOrderLeft - 15, height - distanceOrderAbove + 10, 250, 0.5F);
        cont.fillRect(distanceOrderLeft - 15, height - distanceOrderAbove - 5, 250, 0.5F);

        cont.setNonStrokingColor(Color.LIGHT_GRAY);
        cont.fillRect(distanceOrderLeft - 15, height - distanceOrderAbove - 4.5f, 250, 14.5f);

        cont.setNonStrokingColor(Color.BLACK);

        addText(cont, "BESTELLDATEN", distanceOrderLeft, height - distanceOrderAbove, true, 10);

        //------------------------------------------------------------
        distanceOrderAbove += 20;
        addText(cont, "BESTELL-ID:", distanceOrderLeft, height - distanceOrderAbove, true, 8);
        addText(cont, order.getOrderId().toString(), distanceOrderLeft + 80, height - distanceOrderAbove, false, 8);

        distanceOrderAbove += 20;
        addText(cont, "BESTELLDATUM:", distanceOrderLeft, height - distanceOrderAbove, true, 8);
        addText(cont, StringPrettyPrinter.prettyPrintDate(order.getOrderDate()), distanceOrderLeft + 80,
                height - distanceOrderAbove, false, 8);

        distanceOrderAbove += 20;
        addText(cont, "LIEFERADRESSE:", distanceOrderLeft, height - distanceOrderAbove, true, 8);
        addText(cont, order.getShippingAddress().getStreet() + " " + order.getShippingAddress().getHouseNumber(),
                distanceOrderLeft + 80, height - distanceOrderAbove, false, 8);
        distanceOrderAbove += 20;
        addText(cont, order.getShippingAddress().getPostalCode().toString(), distanceOrderLeft + 80,
                height - distanceOrderAbove, false, 8);

        distanceOrderAbove += 20;
        addText(cont, "BEZAHLT:", distanceOrderLeft, height - distanceOrderAbove, true, 8);
        if (order.isPaid()) {
            addText(cont, "JA", distanceOrderLeft + 80, height - distanceOrderAbove, false, 8);
        } else {
            addText(cont, "NEIN", distanceOrderLeft + 80, height - distanceOrderAbove, false, 8);
        }
    }

    /**
     * method to add all articles from order
     *
     * @param order                    {@link Order}
     * @param document                 {@link PDDocument}
     * @param cont                     {@link PDPageContentStream}
     * @param height                   height of page
     * @param distanceArticleListAbove distance to the top
     * @param distanceArticleListLeft  distance to the left
     * @return PDPageContentStream, if new page was created
     * @throws IOException
     */
    private static PDPageContentStream addArticleListText(Order order, PDDocument document, PDPageContentStream cont,
                                                          float height, int distanceArticleListAbove,
                                                          int distanceArticleListLeft) throws IOException {
        addArticleListHeader(cont, height, distanceArticleListAbove, distanceArticleListLeft);

        //set distances
        //copy this distance, to be able to jump to that point again
        int distanceLeft = distanceArticleListLeft + 2;
        distanceArticleListAbove += 20;

        //add all article in list
        for (OrderArticle orderArticle : order.getArticleList()) {
            //check if this article can be on this page or on the next
            String articleName = orderArticle.getArticle().getArticleName();
            //calculate the width of the articlename
            float textWidth = PDType1Font.TIMES_ROMAN.getStringWidth(articleName) / 1000 * 8;
            //calculate the sections with width 200
            int sections = (int) textWidth / 200 + 1;

            //if there are not at least 30 -> next page
            if (height - distanceArticleListAbove - (sections * 20) <= 30) {
                //create a new page
                cont.close();

                //print header again
                PDPage newPage = new PDPage(PDRectangle.A4);
                document.addPage(newPage);
                cont = new PDPageContentStream(document, newPage);
                distanceArticleListAbove = 40;
                distanceLeft -= 2;
                addArticleListHeader(cont, height, distanceArticleListAbove, distanceArticleListLeft);
                distanceArticleListAbove += 20;
                distanceLeft += 2;
            }

            //add article id
            addText(cont, orderArticle.getArticle().getArticleId().toString(), distanceLeft,
                    height - distanceArticleListAbove, false, 8);

            distanceLeft += 60;

            //copy the distance to the top, if the article name is split in many sections we need the distance for
            // example for the price
            int distanceAboveCopyArticleName = distanceArticleListAbove;
            //calculate all fractures
            List<String> articleNameList =
                    Lists.newArrayList(Splitter.fixedLength(articleName.length() / sections + 1).split(articleName));
            //add all fractures
            for (int x = 0; x < articleNameList.size(); x++) {
                String text = articleNameList.get(x);
                //if it is the last fractures there is no need for -
                if (x < articleNameList.size() - 1) {
                    text = text + "-";
                }
                addText(cont, text, distanceLeft, height - distanceAboveCopyArticleName, false, 8);
                distanceAboveCopyArticleName += 20;
            }

            //add quantity
            distanceLeft += 200;
            addText(cont, orderArticle.getQuantity().toString(), distanceLeft, height - distanceArticleListAbove, false, 8);

            //add price for the article
            distanceLeft += 60;
            addText(cont,
                    StringPrettyPrinter.prettyPrintLongToEuro(orderArticle.getPeriodPrice()), distanceLeft, height - distanceArticleListAbove, false, 8);

            //add the total sum
            distanceLeft += 80;
            long totalPrice =
                    orderArticle.getQuantity() * orderArticle.getPeriodPrice();
            addText(cont, StringPrettyPrinter.prettyPrintLongToEuro(totalPrice), distanceLeft,
                    height - distanceArticleListAbove, false, 8);

            //set the distance to the tip to the copy, because now the price ... is set
            distanceArticleListAbove = distanceAboveCopyArticleName;
            distanceLeft = distanceArticleListLeft + 2;
        }

        //set the var for use in next method
        DISTANCE_ABOVE = distanceArticleListAbove;
        return cont;
    }

    /**
     * method to add the article header
     *
     * @param cont                     {@link PDPageContentStream}
     * @param height                   float
     * @param distanceArticleListAbove distance to top
     * @param distanceArticleListLeft  distance to the left
     * @throws IOException
     */
    private static void addArticleListHeader(PDPageContentStream cont, float height, int distanceArticleListAbove,
                                             int distanceArticleListLeft) throws IOException {
        distanceArticleListLeft += 2;
        addText(cont, "ART.-NR.", distanceArticleListLeft, height - distanceArticleListAbove, false, 8);

        distanceArticleListLeft += 60;
        addText(cont, "BESCHREIBUNG", distanceArticleListLeft, height - distanceArticleListAbove, false, 8);

        distanceArticleListLeft += 200;
        addText(cont, "MENGE", distanceArticleListLeft, height - distanceArticleListAbove, false, 8);

        distanceArticleListLeft += 60;
        addText(cont, "STÜCKPREIS", distanceArticleListLeft, height - distanceArticleListAbove, false, 8);

        distanceArticleListLeft += 80;
        addText(cont, "GESAMTPREIS", distanceArticleListLeft, height - distanceArticleListAbove, false, 8);
    }

    /**
     * method to add the total sum
     *
     * @param document
     * @param cont
     * @param height
     * @param totalPrice
     * @throws IOException
     */
    private static void addTotalSum(PDDocument document, PDPageContentStream cont, float height, String totalPrice) throws IOException {
        int distanceAbove = DISTANCE_ABOVE + 50;
        if (height - distanceAbove <= 60) {
            //create new page
            cont.close();

            PDPage newPage = new PDPage(PDRectangle.A4);
            document.addPage(newPage);
            cont = new PDPageContentStream(document, newPage);
            distanceAbove = 80;
        }

        addText(cont, "VERSAND:", 332, height - distanceAbove, false, 8);
        addText(cont, "KOSTENLOS", 432, height - distanceAbove, false, 8);

        distanceAbove += 5;

        cont.moveTo(322, height - distanceAbove);
        cont.lineTo(502, height - distanceAbove);
        cont.stroke();

        distanceAbove += 18;
        addText(cont, "GESAMTSUMME:", 332, height - distanceAbove, false, 10);
        addText(cont, totalPrice, 432, height - distanceAbove, false, 10);

        cont.close();
    }
}
