package de.bachelorarbeit.utilities.alert;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Class to create a custom alert in one line
 * Pro: Dont need to do line 16-19 every time
 */
public abstract class CustomAlert {

    /**
     * Method to create a custom alert in one line
     * @param alertType type of alert
     * @param title title of alert
     * @param body body message of alert
     * @param header header message of alert
     * @return optional button type, if users presses okay or cancel ..
     */
    public static Optional<ButtonType> createAlert(Alert.AlertType alertType, String title, String body,
                                                   String header) {
        //create alert and set parameters
        Alert alert = new Alert(alertType);
        alert.setHeaderText(header);
        alert.setContentText(body);
        alert.setTitle(title);

        //return value of button press
        return alert.showAndWait();
    }
}
