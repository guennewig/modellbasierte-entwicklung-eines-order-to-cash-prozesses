package de.bachelorarbeit.utilities.mail;


import com.google.common.io.Resources;
import com.sun.mail.util.MailConnectException;
import de.bachelorarbeit.entity.Order;
import de.bachelorarbeit.utilities.alert.CustomAlert;
import de.bachelorarbeit.utilities.logging.LoggingManager;
import de.bachelorarbeit.utilities.pdf.PDFOrderCreator;
import de.bachelorarbeit.utilities.prettyprint.StringPrettyPrinter;
import javafx.scene.control.Alert;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;

/**
 * class to send mails
 */
public abstract class MailSender {

    /**
     * The Mail session.
     */
    private static Session MAIL_SESSION;

    /**
     * username of mail account
     **/
    private static String USERNAME;

    /**
     * sender name
     **/
    private static String SENDER;

    /**
     * method to send a confirmation mail
     * @param order order to be confirm
     * @return true, if email could be send
     */
    public static boolean sendOrderConfirmationEmail(Order order) {
        //load message from resources and change variables
        String message;
        try {
            message = Resources.toString(Resources.getResource("de/bachelorarbeit/mail/templates/order-confirmation" +
                    ".html"), StandardCharsets.UTF_8);
            message = message.replace("_bestellID_", "#" + order.getOrderId());
            message = message.replace("_bestelldatum_", StringPrettyPrinter.prettyPrintDate(order.getOrderDate()));
            message = message.replace("_kunde_", order.getCustomer().getEmail());

            //create a temporary path for the pdf file and create pdf for order
            Path tempPath = PDFOrderCreator.createOrderPDFForEmail(order);
            if (tempPath != null) {
                boolean successful = MailSender.sendMail(tempPath, order.getCustomer().getEmail(), "Bestellbest" +
                        "ätigung", message);
                //delete temp file
                File deleteFile = tempPath.toFile();
                if (deleteFile.isFile()) {
                    successful = successful && deleteFile.delete();
                    return successful;
                }else{
                    return false;
                }
            }
        } catch (IOException e) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler E-Mail Versand!",
                    "Es ist ein Fehler beim E-Mail " + "Versand aufgetreten! (Vorlage laden)", "Fehler E-Mail " +
                            "Versand!");
            LoggingManager.log(Level.ALL, "Fehler E-Mail Versand!", e.getMessage());
        }
        return false;
    }

    /**
     * method to send a mail
     *
     * @param tempFile           pdf bill
     * @param receiverAddresses, receiver email address
     * @param subject            subject
     * @param emailMessage       message
     * @return
     */
    private static boolean sendMail(Path tempFile, String receiverAddresses, String subject, String emailMessage) {
        try {
            if (MailSender.MAIL_SESSION == null) {
                //login into mail account
                MailSender.login();
            }

            //create new mail
            MimeMessage message = new MimeMessage(MailSender.MAIL_SESSION);
            //multipart to have text and pdf
            MimeMultipart mulitpart = new MimeMultipart();

            message.addHeader("Content-type", "text/HTML; charset=UTF-8");

            message.addHeader("format", "flowed");
            message.addHeader("Content-Transfer-Encoding", "8bit");
            message.setFrom(new InternetAddress(MailSender.USERNAME, MailSender.SENDER));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receiverAddresses));
            message.setSubject(subject, "UTF-8");
            message.setSentDate(new Date());

            //add text content
            MimeBodyPart textContent = new MimeBodyPart();
            textContent.setContent(emailMessage, "text/html; charset=UTF-8");
            mulitpart.addBodyPart(textContent);

            //add pdf
            MimeBodyPart attachment = new MimeBodyPart();
            if(tempFile != null){
                attachment.attachFile(tempFile.toFile());
                attachment.setFileName("Bestellbestätigung.pdf");
                mulitpart.addBodyPart(attachment);
            }

            message.setContent(mulitpart);
            //send email
            Transport.send(message);
        } catch (MailConnectException e) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler Verbindung!",
                    "Es konnte keine E-Mail Verbindung " + "hergestellt werden!", "Fehler Verbindung!");
            LoggingManager.log(Level.ALL, "Es konnte keine E-Mail Verbindung hergestellt werden!", e.getMessage());
            return false;
        } catch (Exception e) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler E-Mail Versand!",
                    "Es ist ein Fehler beim E-Mail " + "Versand aufgetreten!", "Fehler E-Mail Versand!");
            LoggingManager.log(Level.ALL, "Fehler E-Mail Versand!", e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * method to login into email account
     * @throws IOException
     */
    public static void login() throws IOException {
        Properties systemProps = new Properties();
        systemProps.load(MailSender.class.getClassLoader().getResourceAsStream("de/bachelorarbeit/mail/mail" +
                ".properties"));

        MailSender.USERNAME = systemProps.getProperty("emailSender");
        MailSender.SENDER = systemProps.getProperty("sender");

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(MailSender.USERNAME, systemProps.getProperty("password"));
            }
        };

        MailSender.MAIL_SESSION = Session.getDefaultInstance(props, auth);
    }
}
