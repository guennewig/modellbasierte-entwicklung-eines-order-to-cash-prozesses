package de.bachelorarbeit.database;

import de.bachelorarbeit.entity.*;
import de.bachelorarbeit.hibernate.config.HibernateUtil;
import de.bachelorarbeit.utilities.alert.CustomAlert;
import de.bachelorarbeit.utilities.logging.LoggingManager;
import javafx.scene.control.Alert;
import org.hibernate.Session;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/**
 * class for all database activities
 * manages all queries of the application
 */
public class DataBaseManager {

    /**
     * instance of the databasemanager as singleton pattern
     **/
    private static DataBaseManager INSTANCE;

    /**
     * instance of the database session, filled with {@link HibernateUtil}
     **/
    private Session databaseSession;

    /**
     * Singleton-Pattern
     *
     * @return single instance of {@link DataBaseManager}
     */
    public static DataBaseManager getInstance() {
        if (DataBaseManager.INSTANCE == null) {
            DataBaseManager.INSTANCE = new DataBaseManager();
        }
        return DataBaseManager.INSTANCE;
    }

    /**
     * is called in {@link de.bachelorarbeit.gui.mainwindow.MainApplication}
     * DataBaseManager.getInstance() to get a database session and to start the connection
     * Pro: when the user does the first query the connection is ready
     */
    public static void startDataBase() {
        DataBaseManager.getInstance();
    }

    /**
     * get a database session from HibernateUtil.getSession()
     * save database session in local variable
     */
    private DataBaseManager() {
        this.databaseSession = HibernateUtil.getSession();
    }

    /**
     * method to shutdown database connection
     */
    public static void shutDown() {
        HibernateUtil.shutdown();
        LoggingManager.info("Datenbankverbindung getrennt");
    }

    /**
     * method to get an entity object by id from the database
     *
     * @param id    id of entity
     * @param clazz type of class
     * @param <T>   entity must have the marker interface DataBaseEntityMarker
     * @return entity T
     */
    public <T extends DataBaseEntityMarker> T getById(Integer id, Class<T> clazz) {
        return this.databaseSession.get(clazz, id);
    }

    /**
     * method to save an entity object
     *
     * @param dataBaseObject entity object of type T
     * @param <T>            entity must have the marker interface DataBaseEntityMarker
     */
    public <T extends DataBaseEntityMarker> void save(T dataBaseObject) {
        //validator checks if all restrictions e.g. NotNull is okay
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(dataBaseObject);
        if (constraintViolations.size() == 0) {
            //save entity object in database
            this.databaseSession.beginTransaction();
            this.databaseSession.save(dataBaseObject);
            this.databaseSession.getTransaction().commit();
        } else {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler Speicherung!",
                    "Bei der Speicherung des Objekts: " + dataBaseObject.toString() + " ist ein Fehler aufgetreten!",
                    "Fehler Speicherung!");
            //log the print messages
            constraintViolations.stream().forEach(violation -> LoggingManager.severe(violation.getMessage()));
        }
    }

    /**
     * Method to update an entity object
     *
     * @param dataBaseObject entity object of type T
     * @param <T>            entity must have the marker interface DataBaseUpdateMarker
     */
    public <T extends DataBaseUpdateMarker> void update(T dataBaseObject) {
        //validator checks if all restrictions e.g. NotNull is okay
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<T>> violations = validator.validate(dataBaseObject);
        if (violations.size() == 0) {
            //save entity object in database
            this.databaseSession.beginTransaction();
            this.databaseSession.saveOrUpdate(dataBaseObject);
            this.databaseSession.getTransaction().commit();
        } else {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler Aktualisierung!", "Bei der Aktualisierung des " +
                    "Objekts: " + dataBaseObject.toString() + " ist ein Fehler aufgetreten!", "Fehler Aktualisierung!");
            //log the print messages
            violations.forEach(violation -> LoggingManager.severe(violation.getMessage()));
        }
    }

    /**
     * method to get a postal code object from database with the postal code number and the city
     * both have to match
     *
     * @param postalCode number of postal code
     * @param cityName   name of city
     * @return if combination of postal code and name of the city exists the entity object, else null
     */
    public PostalCode getPostalCode(String postalCode, String cityName) {
        return (PostalCode) this.databaseSession.createNamedQuery("PostalCode.check").setParameter("postalCode",
                postalCode).setParameter("cityName", cityName).uniqueResult();
    }

    /**
     * method to check if an address is present in the database
     * this must be done, because if the address is present in the database, hibernate needs the entity with id to
     * save another entity with this address
     *
     * @param address address to check
     * @return address, if address is present in database with id, otherwise without id
     */
    public Address checkAddress(Address address) {
        //create a named query and set the parameter
        Address dbAddress = (Address) this.databaseSession.createNamedQuery("Address.check").setParameter("postalCode"
                , address.getPostalCode()).setParameter("street", address.getStreet()).setParameter("houseNumber",
                address.getHouseNumber()).uniqueResult();

        //if dbAddress is null, the address is not present in the database, then return the input
        return dbAddress != null ? dbAddress : address;
    }

    /**
     * method to check if a customer e-mail already exist
     *
     * @param email String email of customer
     * @return true, if customer email already exist, otherwise false
     */
    public boolean isEmailUnique(String email) {
        //create named query and set parameter, if result is null, then the email is not present in the database ->
        // return true
        //check for unique key in database
        return (this.databaseSession.createNamedQuery("Customer.uniqueCheck").setParameter("email", email).uniqueResult()) == null;
    }

    /**
     * method to check if an article name is already taken
     *
     * @param articleName String name of article
     * @return true, if article name is not present in the database, otherwise false
     */
    public boolean isArticleNameUnique(String articleName) {
        //create named query and set parameter
        //if result is null, the article name is not in the database
        //check for unique key in database
        return (this.databaseSession.createNamedQuery("Article.uniqueCheck").setParameter("articleName", articleName).uniqueResult()) == null;
    }

    /**
     * method to delete a entity object
     *
     * @param dataBaseObject entity object present in database
     * @param <T>            entity must have the marker interface DataBaseDeleteMarker
     */
    public <T extends DataBaseDeleteMarker> void deleteByObject(T dataBaseObject) {
        this.databaseSession.beginTransaction();
        dataBaseObject.setDeleted(true);
        this.databaseSession.update(dataBaseObject);
        this.databaseSession.getTransaction().commit();
    }

    /**
     * method to get all customers with firstname, lastname and email from database
     *
     * @param firstName String firstname
     * @param lastName  String lastname
     * @param email     String email
     * @return list of all customer entities that match the search
     */
    public List<Customer> getAllCustomersFromSearch(String firstName, String lastName, String email) {
        //must be done for sql search: LIKE %Test%, Test is input for example for firstname
        firstName = "%" + firstName + "%";
        lastName = "%" + lastName + "%";
        email = "%" + email + "%";
        //create named query and set parameter
        return this.databaseSession.createNamedQuery("Customer.search").setParameter("firstName", firstName)
                .setParameter("lastName", lastName).setParameter("email", email).getResultList();
    }

    /**
     * method to get alle articles from database with the article name
     *
     * @param articleName String name of article
     * @return list of articles that match the search
     */
    public List<Article> getAllArticlesFromSearch(String articleName) {
        //must be done for sql search: LIKE %Test%, Test is input for example for articleName
        articleName = "%" + articleName + "%";
        //create named query and set parameter
        return this.databaseSession.createNamedQuery("Article.search").setParameter("articleName", articleName).getResultList();
    }

    /**
     * method to get all orders from database with the customers lastname, the order date and if the order is paid
     *
     * @param customerLastName String name of customer (lastname)
     * @param orderDate        LocalDate date of order
     * @param paid             boolean if order is already paid
     * @return list of orders from the database that match the search
     */
    public List<Order> getAllOrdersFromSearch(String customerLastName, LocalDate orderDate, boolean paid) {
        //must be done for sql search: LIKE %Test%, Test is input for example for customerLastName
        customerLastName = "%" + customerLastName + "%";
        //for a date there is no LIKE sql operator, so it must be decided if the search is with or without the date
        //create named query and set parameters
        if (orderDate != null) {
            return this.databaseSession.createNamedQuery("Order.searchWithOrderDate").setParameter(
                    "customerLastName", customerLastName).setParameter("orderDateStart", orderDate.atStartOfDay()).
                    setParameter("orderDateEnd", orderDate.plusDays(1).atStartOfDay()).setParameter("paid",
                    paid).getResultList();
        } else {
            return this.databaseSession.createNamedQuery("Order.searchWithOutOrderDate").setParameter(
                    "customerLastName", customerLastName).setParameter("paid", paid).getResultList();
        }
    }

    /**
     * method to reduce the stock quantity after a order was confirmed
     *
     * @param orderArticleList list of all articles of an order
     */
    public void reduceStock(List<OrderArticle> orderArticleList) {
        //iterate through all articles in that order
        for (OrderArticle orderArticle : orderArticleList) {
            //get the article
            Article article = orderArticle.getArticle();
            //reduce the stock quantity of the aticle, deduct the order quantity of this article
            article.setStockQuantity(article.getStockQuantity() - orderArticle.getQuantity());
            //update the article, save the changed quantity to database
            this.update(article);
        }
    }
}
