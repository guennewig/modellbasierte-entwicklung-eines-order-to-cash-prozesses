package de.bachelorarbeit.database;

/**
 * interface marker for entities
 * marks if an entity can be updated
 * needed in {@link DataBaseManager}
 */
public interface DataBaseUpdateMarker {
}
