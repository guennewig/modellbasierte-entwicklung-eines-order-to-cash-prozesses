package de.bachelorarbeit.database;

/**
 * interface marker for entities
 * marks if an entity can be deleted
 * needed in {@link DataBaseManager}
 */
public interface DataBaseDeleteMarker {
    /**
     * Method to set the entity deleted
     * @param deleted
     */
    void setDeleted(boolean deleted);

}
