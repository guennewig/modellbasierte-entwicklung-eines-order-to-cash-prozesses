package de.bachelorarbeit.database;

/**
 * interface marker for entities
 * marks if object is an database entity
 * needed in {@link DataBaseManager}
 */
public interface DataBaseEntityMarker {
}
