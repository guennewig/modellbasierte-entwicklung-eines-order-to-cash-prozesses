package de.bachelorarbeit.entity;


import de.bachelorarbeit.database.DataBaseEntityMarker;
import de.bachelorarbeit.database.DataBaseDeleteMarker;
import de.bachelorarbeit.database.DataBaseUpdateMarker;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * class represents the customer entity in the database, implements DataBaseEntityMarker, DataBaseDeleteMarker,
 * DataBaseUpdateMarker
 * NamedQuery: Customer.uniqueCheck used in {@link de.bachelorarbeit.database.DataBaseManager}
 *             Customer.search used in {@link de.bachelorarbeit.database.DataBaseManager}
 */
@NamedQueries({
        @NamedQuery(name = "Customer.uniqueCheck", query = "Select customer from Customer customer where customer.email " +
        "=" + " :email"),
        @NamedQuery(name = "Customer.search", query = "Select customer from Customer customer where " +
        "customer.firstName like :firstName AND customer.lastName like :lastName AND customer.email like :email AND customer.deleted=false")})
@Entity
@Table(name = "customers")
public class Customer implements DataBaseEntityMarker, DataBaseDeleteMarker, DataBaseUpdateMarker {

    /** databse id of the customer **/
    @Id
    @Column(name = "customer_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer customerId;

    /** address of the customer, one customer has one address, one address has many customers **/
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", nullable = false)
    @NotNull(message = "Adresse muss gefüllt sein.")
    private Address address;

    /** firstname of the customer **/
    @Column(name = "first_name", nullable = false)
    @NotNull(message = "Vorname muss gefüllt sein.")
    private String firstName;

    /** lastname of the customer **/
    @Column(name = "last_name", nullable = false)
    @NotNull(message = "Nachname muss gefüllt sein.")
    private String lastName;

    /** birthday of the customer **/
    @Column(name = "date_of_birth", nullable = false)
    @NotNull(message = "Geburtstag muss gefüllt sein.")
    private LocalDate dateOfBirth;

    /** email of the customer **/
    @Column(name = "email", nullable = false)
    @NotNull(message = "E-Mail muss gefüllt sein.")
    private String email;

    /** if the customer is deleted **/
    @Column(name = "deleted", nullable = false)
    @NotNull(message = "Geloescht muss gefüllt sein.")
    private boolean deleted = false;

    /** constructor, getter and setter after here **/

    /** empty constructor for hibernate **/
    public Customer() {
    }

    public Customer(Address address, String firstName, String lastName, LocalDate dateOfBirth, String email) {
        this.address = address;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.email = email;
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Kunde{" + "kundeId=" + customerId + ", adresse=" + address + ", vorname='" + firstName + '\'' + ", " +
                "nachname='" + lastName + '\'' + ", geburtstag=" + dateOfBirth + ", email='" + email + '\'' + ", " +
                "geloescht=" + deleted + '}';
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
