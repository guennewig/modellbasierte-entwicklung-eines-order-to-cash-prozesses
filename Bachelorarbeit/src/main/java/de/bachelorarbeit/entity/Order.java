package de.bachelorarbeit.entity;

import de.bachelorarbeit.database.DataBaseEntityMarker;
import de.bachelorarbeit.database.DataBaseUpdateMarker;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * class represents the order entity in the database, implements DataBaseEntityMarker,
 * DataBaseUpdateMarker
 * NamedQuery: Order.searchWithOrderDate used in {@link de.bachelorarbeit.database.DataBaseManager}
 * Order.searchWithOutOrderDate used in {@link de.bachelorarbeit.database.DataBaseManager}
 */
@NamedQueries({@NamedQuery(name = "Order.searchWithOrderDate", query = "Select order from Order order where order" +
        ".customer.lastName LIKE :customerLastName AND order.orderDate BETWEEN :orderDateStart AND :orderDateEnd AND order.paid = :paid"),
        @NamedQuery(name = "Order.searchWithOutOrderDate", query = "Select order from Order order where order" +
                ".customer.lastName LIKE :customerLastName AND order.paid = :paid")})
@Entity
@Table(name = "orders")
public class Order implements DataBaseEntityMarker, DataBaseUpdateMarker {

    /** databse id of the order **/
    @Id
    @Column(name = "order_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer orderId;

    /** customer of the order, one order has one customer, one customer has many orders **/
    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    @NotNull(message = "Kunde muss gefüllt sein.")
    private Customer customer;

    /** shipping address of the order, one order has one shipping address, one shipping address has many orders **/
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "shipping_address_id", nullable = false)
    @NotNull(message = "Adresse muss gefüllt sein.")
    private Address shippingAddress;

    /** order date of the order **/
    @Column(name = "order_Date", nullable = false)
    @NotNull(message = "Bestelldatum muss gefüllt sein.")
    private LocalDateTime orderDate;

    /** total price of order */
    @Column(name = "total_price", nullable = false)
    @NotNull(message = "Gesamtpreis muss gefüllt sein.")
    private Long totalPrice;

    /** if the order is paid **/
    @Column(name = "paid", nullable = false)
    @NotNull(message = "Bezahlt muss gefüllt sein.")
    private boolean paid = false;

    /** articles that the order contains, one order has many articles, one article has one order **/
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    @NotNull
    @OrderBy("article.articleId ASC")
    @NotEmpty(message = "Liste der Artikel muss mindestens einen Artikel enthalten.")
    private List<OrderArticle> articleList;

    /** constructor, getter and setter after here **/

    /** empty constructor for hibernate **/
    public Order() {
    }

    public Order(Customer customer, LocalDateTime orderDate) {
        this.customer = customer;
        this.orderDate = orderDate;
    }

    @Override
    public String toString() {
        return "Bestellung{" + "bestellungId=" + orderId + ", kunde=" + customer + ", adresse=" + shippingAddress +
                ", bestellDatum=" + orderDate + ", bezahlt=" + paid + ", artikelListe=" + articleList + '}';
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public List<OrderArticle> getArticleList() {
        return articleList;
    }

    public void setArticleList(List<OrderArticle> articleList) {
        this.articleList = articleList;
    }
}
