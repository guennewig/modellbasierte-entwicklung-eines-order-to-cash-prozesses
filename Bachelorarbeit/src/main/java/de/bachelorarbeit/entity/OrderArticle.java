package de.bachelorarbeit.entity;

import de.bachelorarbeit.database.DataBaseEntityMarker;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * class represents the order article entity in the database, implements DataBaseEntityMarker
 */
@Entity
@Table(name = "order_articles")
public class OrderArticle implements Serializable, DataBaseEntityMarker {

    /**
     * databse primary key of the order article
     **/
    @Id
    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;

    /**
     * databse primary key of the order article
     **/
    @Id
    @ManyToOne
    @JoinColumn(name = "article_id", nullable = false)
    @NotNull(message = "Artikel muss gefüllt sein.")
    private Article article;

    /**
     * quantity of the article in the order
     **/
    @Column(name = "quantity", nullable = false)
    @NotNull(message = "Menge muss gefüllt sein.")
    private Long quantity;

    /**
     * Price valid at the time of order
     **/
    @Column(name = "period_price", nullable = false)
    @NotNull(message = "Artikelpreis muss gefüllt sein.")
    private Long periodPrice;

    /** constructor, getter and setter after here **/

    /**
     * empty constructor for hibernate
     **/
    public OrderArticle() {
    }

    public OrderArticle(Article article, Long quantity, Long periodPrice) {
        this.article = article;
        this.quantity = quantity;
        this.periodPrice = periodPrice;
    }

    @Override
    public String toString() {
        return "BestellungArtikel{" +
                "artikel=" + article +
                ", menge=" + quantity +
                '}';
    }

    public Order getOrder() {
        return this.order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Article getArticle() {
        return this.article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Long getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getPeriodPrice() {
        return this.periodPrice;
    }

    public void setPeriodPrice(Long periodPrice) {
        this.periodPrice = periodPrice;
    }
}
