package de.bachelorarbeit.entity;

import de.bachelorarbeit.database.DataBaseEntityMarker;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * class represents the address entity in the database, implements DataBaseEntityMarker
 * NamedQuery: Address.check used in {@link de.bachelorarbeit.database.DataBaseManager}
 */
@NamedQuery(name = "Address.check", query = "Select address from Address address where address.postalCode = " +
        ":postalCode " + "AND address.street = :street " + "AND address.houseNumber = :houseNumber")
@Entity
@Table(name = "addresses")
public class Address implements DataBaseEntityMarker {

    /**
     * databse id of the address
     **/
    @Id
    @Column(name = "address_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer addressId;

    /**
     * the postal code of the address, one address has one postal code, one postal code has many addresses
     **/
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "postal_code_id", nullable = false)
    @NotNull(message = "Postleitzahl muss gefüllt sein.")
    private PostalCode postalCode;

    /** street of the address **/
    @Column(name = "street", nullable = false)
    @NotNull(message = "Strasse muss gefüllt sein.")
    private String street;

    /** house number of the address **/
    @Column(name = "house_number", nullable = false)
    @NotNull(message = "Hausnummer muss gefüllt sein.")
    private String houseNumber;


    /** constructor, getter and setter after here **/


    /** emtpy constructor for hibernate **/
    public Address() {
    }

    public Address(PostalCode postalCode, String street, String houseNumber) {
        this.postalCode = postalCode;
        this.houseNumber = houseNumber;
        this.street = street;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public PostalCode getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(PostalCode postalCode) {
        this.postalCode = postalCode;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Override
    public String toString() {
        return this.getStreet() + " " + this.getHouseNumber() + ", " + this.getPostalCode().getCityName() + " (" + this.getPostalCode().getPostalCode() + ")";
    }
}
