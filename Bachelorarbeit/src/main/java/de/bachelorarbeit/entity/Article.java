package de.bachelorarbeit.entity;

import de.bachelorarbeit.database.DataBaseDeleteMarker;
import de.bachelorarbeit.database.DataBaseEntityMarker;
import de.bachelorarbeit.database.DataBaseUpdateMarker;
import de.bachelorarbeit.utilities.alert.CustomAlert;
import de.bachelorarbeit.utilities.logging.LoggingManager;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;

/**
 * class represents the article entity in the database, implements DataBaseEntityMarker, DataBaseDeleteMarker,
 * DataBaseUpdateMarker
 * NamedQuery: Article.uniqueName used in {@link de.bachelorarbeit.database.DataBaseManager}
 * Article.search used in {@link de.bachelorarbeit.database.DataBaseManager}
 */
@NamedQueries({@NamedQuery(name = "Article.uniqueCheck", query = "Select article from Article article where " +
        "article.articleName = :articleName"), @NamedQuery(name = "Article.search", query = "Select article from " +
        "Article article where " + "article" + ".articleName like " + ":articleName AND article" + ".deleted=false")})
@Entity
@Table(name = "articles")
public class Article implements DataBaseEntityMarker, DataBaseDeleteMarker, DataBaseUpdateMarker {

    /**
     * databse id of the article
     **/
    @Id
    @Column(name = "article_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer articleId;

    /**
     * name of the article
     **/
    @Column(name = "article_name", nullable = false)
    @NotNull(message = "Es muss eine Artikelbezeichnung eingetragen sein.")
    private String articleName;

    /**
     * stock quantity of the article
     **/
    @Column(name = "stock_quantity", nullable = false)
    @NotNull(message = "Die Lagermenge muss gefüllt sein.")
    private Long stockQuantity;

    /**
     * if article is deleted
     **/
    @Column(name = "deleted", nullable = false)
    @NotNull(message = "Das Attribut geloescht darf nicht null sein.")
    private boolean deleted = false;

    /**
     * picture of the article
     **/
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "article_picture")
    private byte[] articlePicture;

    /**
     * prices of the article, one article has many prices, one price has one article, ordered by the date in the price
     **/
    @OneToMany(mappedBy = "article", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @OrderBy("validityDate ASC")
    @NotNull(message = "Die Liste der Preise darf nicht null sein.")
    @NotEmpty(message = "Die Liste der Preise darf nicht leer sein. Es muss mindestens ein Preis eingetragen sein.")
    private List<ArticlePrice> prices;

    /**
     * empty constructor for hibernate
     **/
    public Article() {
    }

    public Article(String articleName, Long stockQuantity, List<ArticlePrice> prices) {
        this.articleName = articleName;
        this.stockQuantity = stockQuantity;
        this.prices = prices;
    }

    /**
     * method to get the current price of the article
     *
     * @return ArticlePrice current price
     */
    public ArticlePrice getCurrentPrice() {
        //sort prices with date
        this.getPrices().sort(Comparator.comparing(ArticlePrice::getValidityDate));

        ArticlePrice selectedPrice = null;
        //iterate through all prices
        for (ArticlePrice articlePrice : this.getPrices()) {
            //find the price that is nearest to the requested date or equal
            if (articlePrice.getValidityDate().isBefore(LocalDateTime.now().toLocalDate()) ||
                    articlePrice.getValidityDate().isEqual(LocalDateTime.now().toLocalDate())) {
                selectedPrice = articlePrice;
            }
        }

        if (selectedPrice == null) {
            CustomAlert.createAlert(Alert.AlertType.ERROR,
                    "Fehler Preisfindung!", "Es konnte kein passender Preis gefunden werden!", "Fehler Preisfindung!");
            return null;
        }

        return selectedPrice;
    }

    /**
     * constructor, getter and setter after here
     **/

    /**
     * method to set the image into a imageview
     *
     * @param imageview imageview where the image should show up
     */
    public void setImageToView(ImageView imageview) {
        boolean pictureNotFoundOrFailure = false;
        //if the article has no picture -> print dummy picture
        if (this.getArticlePicture() != null) {
            //get ByteArrayInputStream from the article picture
            ByteArrayInputStream bis = new ByteArrayInputStream(this.getArticlePicture());
            BufferedImage image = null;
            try {
                image = ImageIO.read(bis);
            } catch (IOException e) {
                pictureNotFoundOrFailure = true;
            }

            if (image != null) {
                //set the image to the imageview
                imageview.setImage(SwingFXUtils.toFXImage(image, null));
            }
        } else {
            pictureNotFoundOrFailure = true;
        }

        if(pictureNotFoundOrFailure){
            //get the dummy picture from the resources and set it
            imageview.setImage(new Image(this.getClass().getClassLoader().getResourceAsStream("de/bachelorarbeit/gui" + "/images/no_photo.png")));
        }
    }

    /**
     * method to save a picture file into the local variable
     *
     * @param picture picture to save
     * @throws IOException
     */
    public void saveImage(File picture) {
        try {
            BufferedImage image = ImageIO.read(picture);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", baos);
            this.setArticlePicture(baos.toByteArray());
        } catch (IOException e) {
            CustomAlert.createAlert(Alert.AlertType.ERROR, "Fehler Artikelbild speichern!", "Beim Speichern des " +
                    "Artikelbilds ist ein Fehler aufgetreten!", "Fehler Artikelbild speichern!");
            LoggingManager.log(Level.ALL, "Fehler Speichern Artikelbild!", e.getMessage());
        }
    }

    @Override
    public String toString() {
        return "Artikel{" + "artikelId=" + articleId + ", artikelBezeichnung='" + articleName + '\'' + ", " +
                "lagermenge=" + stockQuantity + ", geloescht=" + deleted + ", preise=" + prices + '}';
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public Long getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Long stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public byte[] getArticlePicture() {
        return articlePicture;
    }

    public void setArticlePicture(byte[] articlePicture) {
        this.articlePicture = articlePicture;
    }

    public List<ArticlePrice> getPrices() {
        return prices;
    }

    public void setPrices(List<ArticlePrice> prices) {
        this.prices = prices;
    }
}
