package de.bachelorarbeit.entity;

import de.bachelorarbeit.database.DataBaseEntityMarker;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * class represents the postalcode entity in the database, implements DataBaseEntityMarker
 * NamedQuery: PostalCode.check used in {@link de.bachelorarbeit.database.DataBaseManager}
 */
@NamedQuery(name = "PostalCode.check", query = "Select postalCode from PostalCode postalCode where postalCode.postalCode = " +
                ":postalCode AND postalCode.cityName = :cityName")
@Entity
@Table(name = "postal_codes")
public class PostalCode implements DataBaseEntityMarker {

    /** databse id of the postal code **/
    @Id
    @Column(name = "postal_code_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer postalCodeId;

    /** postal code as string **/
    @Column(name = "postal_code", nullable = false, length = 5)
    @NotNull
    private String postalCode;

    /** name of the city **/
    @Column(name = "city_name", nullable = false)
    @NotNull
    private String cityName;

    /** constructor, getter and setter after here **/

    /** empty constructor **/
    public PostalCode() {
    }

    public PostalCode(String postalCode, String cityName) {
        this.postalCode = postalCode;
        this.cityName = cityName;
    }

    @Override
    public String toString() {
        return this.cityName + " (" + this.postalCode + ")";
    }

    public Integer getPostalCodeId() {
        return postalCodeId;
    }

    public void setPostalCodeId(Integer postalCodeId) {
        this.postalCodeId = postalCodeId;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
