package de.bachelorarbeit.entity;


import de.bachelorarbeit.database.DataBaseEntityMarker;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * class represents the article price entity in the database, implements DataBaseEntityMarker
 */
@Entity
@Table(name = "article_prices")
public class ArticlePrice implements Serializable, DataBaseEntityMarker {

    /** databse primary key of the article price **/
    @Id
    @ManyToOne
    @JoinColumn(name = "article_id", nullable = false)
    private Article article;

    /** databse primary key of the article price **/
    @Id
    @Column(name = "validity_Date", nullable = false)
    @NotNull(message = "Das Datum muss gefüllt sein.")
    private LocalDate validityDate;

    /** price of the article **/
    @Column(name = "price", nullable = false)
    @NotNull(message = "Preis muss gefüllt sein.")
    private Long price;

    /** constructor, getter and setter after here **/

    /** emtpy constructor for hibernate **/
    public ArticlePrice() {
    }

    public ArticlePrice(LocalDate date, Long price) {
        this.validityDate = date;
        this.price = price;
    }

    @Override
    public String toString() {
        return this.validityDate + " - " + this.price;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public LocalDate getValidityDate() {
        return this.validityDate;
    }

    public void setDate(LocalDate date) {
        this.validityDate = date;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}
