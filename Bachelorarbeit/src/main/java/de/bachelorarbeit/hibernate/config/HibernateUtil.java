package de.bachelorarbeit.hibernate.config;

import de.bachelorarbeit.utilities.logging.LoggingManager;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.logging.Level;

/**
 * class to create a database session, used by {@link de.bachelorarbeit.database.DataBaseManager}
 */
public class HibernateUtil {

    /** service registry **/
    private static StandardServiceRegistry REGISTRY;

    /** manages the sessions **/
    private static SessionFactory SESSION_FACTORY;

    /**
     * method to get a session factory
     * @return session factory
     */
    private static SessionFactory getSessionFactory() {
        if (HibernateUtil.SESSION_FACTORY == null) {
            try {
                // Create registry and load config file
                HibernateUtil.REGISTRY = new StandardServiceRegistryBuilder().configure("de/bachelorarbeit/hibernate/config" +
                        "/hibernate.cfg.xml").build();
                // Create MetadataSources
                MetadataSources sources = new MetadataSources(HibernateUtil.REGISTRY);
                // Create Metadata
                Metadata metadata = sources.getMetadataBuilder().build();
                // Create SessionFactory
                HibernateUtil.SESSION_FACTORY = metadata.getSessionFactoryBuilder().build();

                LoggingManager.info("Datenbankverbindung erfolgreich initialisiert!");
            } catch (Exception e) {
                e.printStackTrace();
                LoggingManager.log(Level.ALL, "Fehler bei der Datenbankverbindung!", e.getMessage());
                if (HibernateUtil.REGISTRY != null) {
                    StandardServiceRegistryBuilder.destroy(HibernateUtil.REGISTRY);
                }
            }
        }
        return HibernateUtil.SESSION_FACTORY;
    }

    /**
     * method to get a session
     * @return session
     */
    public static Session getSession() {
        return HibernateUtil.getSessionFactory().openSession();
    }

    /**
     * method to shutdown connection
     */
    public static void shutdown() {
        if (HibernateUtil.REGISTRY != null) {
            StandardServiceRegistryBuilder.destroy(HibernateUtil.REGISTRY);
            HibernateUtil.SESSION_FACTORY = null;
        }
    }
}