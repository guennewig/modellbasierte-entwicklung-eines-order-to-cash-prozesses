<h1 align="center">Modellbasierte Entwicklung eines Order-to-Cash-Prozesses</h3>
</div>

<!-- ABOUT THE PROJECT -->
## Über das Projekt

Das Projekt ist im Rahmen einer Bachelorarbeit an der Ostfalia Hochschule für angewandte Wissenschaften in Wolfenbüttel entstanden.
Es beschäftigt sich mit der modellbasierten Entwicklung eines Order-to-Cash-Prozesses.
Die Applikation bietet die Möglichkeit Artikel, Kunden und Bestellungen anzulegen und diese zu verwalten.

In \Bachelorarbeit ist der Source-Code des Projekts zu finden.

In \Modellierung sind alle erstellten Modellierungen zu finden.

In \Testphotos sind Photos zu finden die für die Applikation verwendet werden können.

Bestellbestätigung.pdf ist eine Beispielsrechnung und E-Mail-Bestellbestätigung.jpg die zugehörige E-Mail.


<p align="right">(<a href="#top">back to top</a>)</p>



### Verwendete Werkzeuge

* [JavaFX](https://openjfx.io/)
* [Maven](https://maven.apache.org/)
* [PostgreSQL](https://www.postgresql.org/)
* [Hibernate](https://hibernate.org/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Erste Schritte

Nachfolgend werden die ersten Schritte beschrieben, um das Projekt zu bauen und zu starten.

### Vorbedingungen

Die Anwendung wurde mit Java 1.8.0_202 entworfen. Daher wird diese Version empfohlen.

Des Weiteren muss eine PostgreSQL Datenbank betrieben werden und die Parameter in der Applikation angepasst werden.

### Installation

1. Clone the repo
   ```sh
   git clone https://github.com/guennewig/modellbasierte-entwicklung-eines-order-to-cash-prozesses.git
   ```
2. Build Jar
   ```sh
   mvn clean package
   ```
3. Start the Bachelorarbeit-Full.jar in \target

<p align="right">(<a href="#top">back to top</a>)</p>
